# track analysis

This repository contains an AthAnalysis algorithm for analysing tracking performance. The following links may be useful if you are new to AthAnalysis:
- [AthAnalysis tutorial](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake) (I use `acm`). 
- [AthAnalysis Handbook](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysis).

# Quick Start


To get started, ssh into lxplus or some tier-3 cluster (note that if you do not run on UCL's plus1 cluster you will have to download input AOD files yourself). Clone the repository:

```
git clone ssh://git@gitlab.cern.ch:7999/svanstro/trackAnalysis.git
```

Enter the respository and run the initialisation script:

```
cd trackAnalysis
source init.sh
```

Hopefully the algorithm compiles and you are landed in the newly created `run/` directory.

You should be able to run a test job using:

```
nohup athena trackAnalysis/trackAnalysisAlgJobOptions.py > logs/test.log &
```

Use `nohup` to ensure that the job continues even if you disconnect from the machine, and route the output from the job into a log file. The test job should finish quickly, have a look at the log file (`less logs/test.log`) to check that everything was completed successfully. The output ROOT file, containing all the histograms generated using the code, is stored under the `output/` folder. Now you can use this file to produce plots (see [here](#producing-plots)).

## Returning

After the initial setup, when you next return to work you should be able to navigate to the `trackAnalysis` folder and run 
```
source setup.sh
```
to be ready to go again.


## Configuring Jobs

You can edit the JobOptions file `source/trackAnalysis/share/trackAnalysisAlgJobOptions.py` 
to configure:
- input AOD files you want to analyse. First add DSIDs to the `datasets` dictionary, and then specify the key using the `DATASET` variable.
- base path (`dataset_base_path`), which is prepended to the DSID when loading the files.
- Ouput ROOT filename, using the `OUTNAME` variable.
- Number of events to process (`theApp.EvtMax`).

Some more options are hardcoded into the source `source/trackAnalysis/src/trackAnalysisAlg.cxx`. Look for the `User Config` section in the `trackAnalysisAlg::initialize()` function. You can set
- running mode (baseline, pseudotracks, etc)
- fast mode (to speed up jobs but get fewer plots)

Make sure that you compile the code using `acm compile` after making any changes to the C++ source.

# Producing Plots

From the `run/` directory, 

```
./make-plots.py ouput/<out-name>/
```

Replace `<out-name>` with the name of the output folder you want to plot. By defualt, if there are several root files in the directory, the plots from each file are overlaid. Plots will end up in a timestamped directory inside `plots/`.

To get the efficiency plots, run `./custom-plot.py` (after pointing the script to the relevant files).

# Running a Reco_tf job

If you want to run your own reconstruction to produce AODs, you need to run a [Reco_tf](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/RecoTf) job, probably using [panda](https://twiki.cern.ch/twiki/bin/view/PanDA/PandaAthena) to submit jobs to the grid.

## Setup

### A: Setup Without Athena Modificaitons

On your lxplus workspace or tier-3 machine:
```
setupATLAS
mkdir reconstruction; cd reconstruction
mkdir build run; cd build
asetup Athena,21.0.100
cd ../run
```

### B: Setup With Athena Modificaitons

See [here](https://atlassoftwaredocs.web.cern.ch/athena/).

Make sure to run `make` from the `build/` dir and then `source ../build/*/setup.sh` from the `run/` dir before running any jobs! (otherwise any modifications you make to the Athena source code will not be applied).

## Running Locally

You will need to make sure the relevant python include scripts are present in the `run/` dir. These scripts are found in the `scripts/` directory of this repository (also [here](https://cernbox.cern.ch/index.php/s/7CiTLzGw6t3Fl3N)).

To run a local test job (good practice to do this before submitting to the grid and finding is broken), you will need a local RDO file to run off. At UCL, you can use mine. To run the transform:
```
 nohup Reco_tf.py \
    --autoConfiguration everything \
    --inputRDOFile /unix/atlas1/svanstroud/qt/rdo/mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.recon.RDO.e6928_e5984_s3431_r11193/RDO.18772079._000001.pool.root.1 \
    --outputAODFile baseline.root \
    --preInclude preInclude.IDonly_reconstruction.py \
    --postInclude postInclude.addMoreInfo.py \
    --ignorePatterns 'Py:Configurable.+ERROR.+attempt.+to.+add.+a.+duplicate.+(AthCondSeq.JetTagCalibCondAlg).' \
    --maxEvents 1 \
    > reco_tf.log &
```

Adding a few things, we can get fill PU truth info, and also [pseudo-tracks](https://twiki.cern.ch/twiki/bin/view/Atlas/InDetPseudoTracking) written out alongside reco tracks:
```
nohup Reco_tf.py \
    --autoConfiguration=everything \
    --inputRDOFile=/unix/atlas1/svanstroud/qt/rdo/mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.recon.RDO.e6928_e5984_s3431_r11193/RDO.18772079._000275.pool.root.1 \
    --outputAODFile=baseline-fullPU.root \
    --preInclude preInclude.IDonly_reconstruction.py \
    --postInclude postInclude.addMoreInfo.pseudotracks.py \
    --preExec 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from LArDigitization.LArDigitizationFlags import jobproperties;jobproperties.LArDigitizationFlags.useEmecIwHighGain.set_Value_and_Lock(False)' 'ESDtoAOD:from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet.set_Value_and_Lock("AODSLIM");' 'RAWtoESD:InDetFlags.doNewTracking.set_Value_and_Lock(True);InDetFlags.doPseudoTracking.set_Value_and_Lock(True);InDetFlags.doSLHC.set_Value_and_Lock(False)' \
    --postExec 'all:CfgMgr.MessageSvc().setError+=["HepMcParticleLink"]' 'ESDtoAOD:xAODMaker__xAODTruthCnvAlg("GEN_AOD2xAOD",WriteInTimePileUpTruth=True);StreamAOD.ForceRead=True;' 'RAWtoESD:xAODMaker__xAODTruthCnvAlg("GEN_AOD2xAOD",WriteInTimePileUpTruth=True);InDetPRD_TruthTrajectoryBuilder.PRD_TruthTrajectoryManipulators=[InDetTruthTrajectorySorter];InDetTruthTrackCreation.PRD_TruthTrajectorySelectors=[];' 'ESDtoDPD:xAODMaker__xAODTruthCnvAlg("GEN_AOD2xAOD",WriteInTimePileUpTruth=True);TruthDecor.DecorationPrefix="IDTIDE_"' 'HITtoRDO:from AthenaCommon.AppMgr import ToolSvc;ToolSvc.PixelDigitizationTool.ToTMinCut=[0,4,6,6,6,0];' 'RAWtoESD:StreamESD.ItemList+="InDetSimDataCollection#*","SiHitCollection#*"' 'HITtoRDO:streamRDO.ItemList+="SiHitCollection#*",' \
    --maxEvents=1 \
    > reco_tf.log &
```


## Running On The Grid

To run on the grid, we need to set up `panda`, `rucio` and a grid proxy.
```
cd ../build
setupATLAS; asetup;
make;
cd ../run
lsetup panda rucio; voms-proxy-init -voms atlas
source ../build/*/setup.sh
```

Then, we can submit jobs to the grid like:
```
reco_tf="Reco_tf.py \
    --autoConfiguration=everything \
    --inputRDOFile %IN \
    --outputAODFile %OUT.AOD.root \
    --preInclude preInclude.IDonly_reconstruction.py \
    --postInclude postInclude.addMoreInfo.pseudotracks.py \
    --preExec 'all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from LArDigitization.LArDigitizationFlags import jobproperties;jobproperties.LArDigitizationFlags.useEmecIwHighGain.set_Value_and_Lock(False)' 'ESDtoAOD:from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet.set_Value_and_Lock(\"AODSLIM\");' 'RAWtoESD:InDetFlags.doNewTracking.set_Value_and_Lock(True);InDetFlags.doPseudoTracking.set_Value_and_Lock(True);InDetFlags.doSLHC.set_Value_and_Lock(False); InDetFlags.doStoreTrackCandidates.set_Value_and_Lock(True)' \
    --postExec 'all:CfgMgr.MessageSvc().setError+=[\"HepMcParticleLink\"]' 'ESDtoAOD:xAODMaker__xAODTruthCnvAlg(\"GEN_AOD2xAOD\",WriteInTimePileUpTruth=True);StreamAOD.ForceRead=True;' 'RAWtoESD:xAODMaker__xAODTruthCnvAlg(\"GEN_AOD2xAOD\",WriteInTimePileUpTruth=True);InDetPRD_TruthTrajectoryBuilder.PRD_TruthTrajectoryManipulators=[InDetTruthTrajectorySorter];InDetTruthTrackCreation.PRD_TruthTrajectorySelectors=[];' 'ESDtoDPD:xAODMaker__xAODTruthCnvAlg(\"GEN_AOD2xAOD\",WriteInTimePileUpTruth=True);TruthDecor.DecorationPrefix=\"IDTIDE_\"' 'HITtoRDO:from AthenaCommon.AppMgr import ToolSvc;ToolSvc.PixelDigitizationTool.ToTMinCut=[0,4,6,6,6,0];' 'RAWtoESD:StreamESD.ItemList+=\"InDetSimDataCollection#*\",\"SiHitCollection#*\"' 'HITtoRDO:streamRDO.ItemList+=\"SiHitCollection#*\",' \
    "; \
pathena \
    --trf "$reco_tf" \
    --inDS mc16_13TeV.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.recon.RDO.e6928_e5984_s3431_r11193 \
    --outDS user.username.outname \
    --extOutFile HitInfo.root \
    --nFilesPerJob 1 \
    --memory 4096 \
    --expertOnly_skipScout \
    --osMatching \
    --nFiles 200 \
```
Make sure to change the `--outDS user.username.outname` option to something like `--outDS user.<your_username>.<intelligable_string>`.