#ifndef TRACKANALYSIS_TRACKANALYSISALG_H
#define TRACKANALYSIS_TRACKANALYSISALG_H 1

// ---------------------------------------------------
// C++ Includes
// ---------------------------------------------------
#include <vector>
#include <unistd.h>
// ---------------------------------------------------
// ROOT Includes
// ---------------------------------------------------
#include "TH1D.h"
#include "TProfile.h"
#include "TTree.h"
// ---------------------------------------------------
// Athena Includes
// ---------------------------------------------------
// base algorithm class
#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

// hist service
#include "GaudiKernel/ITHistSvc.h"

// tool handler
#include "AsgTools/AnaToolHandle.h"

// tools
#include "InDetTrackSystematicsTools/InDetTrackTruthOriginTool.h"
#include "InDetTrackSystematicsTools/InDetTrackTruthOriginDefs.h"
// ---------------------------------------------------
// xAOD Includes
// ---------------------------------------------------
#include "xAODEventInfo/EventInfo.h"

// tracking
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticleAuxContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"

#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODTracking/TrackStateValidationContainer.h"

// truth 
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticleAuxContainer.h"

// calo
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloCluster.h"

// truth links
#include "AthLinks/ElementLink.h"
#include "xAODTracking/TrackStateValidation.h"
// ---------------------------------------------------
// misc
// ---------------------------------------------------
#include "AtlasStyle.C"
//#include "histUtils.h"
//#include "histBooker.cxx"
// ---------------------------------------------------

struct TrackInfo {
  double pt;
  double eta;
  double theta;
  double phi;
  double d0;
  double z0;
  double qOverP;
};

struct HitCount {
  double IBL;
  double B;
  double Pix;
  double SCT;
};



class trackAnalysisAlg: public ::AthAnalysisAlgorithm { 
 public: 
  trackAnalysisAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~trackAnalysisAlg(); 

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  //virtual StatusCode  firstExecute();   //once, after first eventdata is loaded (not per file)
  virtual StatusCode  execute();        //per event
  //virtual StatusCode  endInputFile();   //end of each input file
  //virtual StatusCode  metaDataStop();   //when outputMetaStore is populated by MetaDataTools
  virtual StatusCode  finalize();       //once, after all events processed
  

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp



 private:

  // convience types
  typedef ElementLink<xAOD::TruthParticleContainer> TruthLink;
  typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > > MeasurementsOnTrack;
  typedef std::vector<ElementLink< xAOD::TrackStateValidationContainer > >::const_iterator MeasurementsOnTrackIter;
  typedef std::unordered_map<std::string, float> HitCounts;
  const char* measurementNames = "msosLink"; // "MeaSurement On Surface"

  // inner detector codoes
  const int detPixel = 1;
  const int detSCT   = 2;

  // xAOD containers
  const xAOD::EventInfo* m_ei = nullptr;
  const xAOD::TruthParticleContainer* m_truthParticles = nullptr;
  const xAOD::TrackParticleContainer* m_reconstructedTracks = nullptr;
  const xAOD::TrackParticleContainer* m_pseudotracks = nullptr;
  const xAOD::TrackParticleContainer* m_candidates = nullptr;
  


  // alg config
  std::string m_running_mode;
  std::set<std::string> m_origins;
  std::set<std::string> m_hadrons;
  bool m_fast_mode;

  // hist params
  const int tpT_nbins = 20;
  const float tpT_max   = 500.0;
  const int hpT_nbins = 20;
  const float hpT_max   = 2000.0;
  const int PR_nbins = 18;
  const float PR_max   = 144.0;
  const int dR_nbins = 20;
  const float dR_max   = 0.1;

  // keep track of processed events
  int eventCounter = 0;
  int trackCounter = 0;

  // maps
  std::vector<const xAOD::TruthParticle*> m_reconstructableTruth;
  std::vector<const xAOD::TruthParticle*> m_reconstructedTruth;
  std::vector<const xAOD::TrackMeasurementValidation*> m_clustersOnTracks;
  std::unordered_map<const xAOD::TruthParticle*, const xAOD::TrackParticle*> m_truthIdealMap;
  std::unordered_map<const xAOD::TruthParticle*, std::string> m_truthParticleOriginMap;

  const std::set<std::string> m_dets = {"IBL", "B", "Pix", "SCT"};

  // different hit info
  HitCounts m_total;
  HitCounts m_good;
  HitCounts m_wrong;
  HitCounts m_missing;
  HitCounts m_shared;
  HitCounts m_split;
  HitCounts m_outliers;
  HitCounts m_holes;
  HitCounts m_nTruthOnCluster;

  // particle info
  std::string m_origin;
  TrackInfo m_reco;
  TrackInfo m_truth;
  TrackInfo m_error;
  double m_parent_pt;
  double m_parent_dR;
  double m_prod_rad;
  double m_parent_DL;


  // bookkeeping
  const char* replaceAll(std::string str, const std::string& from, const std::string& to);
  StatusCode bookAllHistograms();
  StatusCode bookHistogram(const std::string origin, const std::string id, const std::string title);
  void fill(std::string id, float x, float y, std::string origin="All");
  void fill(std::string id, float x, std::string origin="All");
  void fillHistogram(const std::string origin, const std::string id, double y_val,
        double track_pt=-1.0, double prod_rad=-1.0, double parent_pt=-1.0, double parent_dR=-1.0);


  // select the track to use as "study" and "ideal" reference tracks
  std::pair <const xAOD::TrackParticleContainer*,const xAOD::TrackParticleContainer*> 
  setupTracks( const xAOD::TruthParticleContainer* TruthParticles,
               const xAOD::TrackParticleContainer* reconstructedTracks,
               const xAOD::TrackParticleContainer* pseudotracks,
               std::string running_mode );

  /*
  Loop over ideal tracks and get truth links. Note that we will only find truth
  links for primary vertex truth particles (unless we have a full pileup truth
  derivation).
  */
  const xAOD::TruthParticle* getTruth( const xAOD::TrackParticle* track );
  void mapTruthReconstructability( const xAOD::TrackParticleContainer* idealTracks );
  
  bool pseudotrackIsReconstructed(const xAOD::TruthParticle* ideaTruth);

  // loop over reconstructed tracks
  void runRecoLoop(const xAOD::TrackParticleContainer* tracks);

  // loop over truth particles
  void runTruthLoop(const xAOD::TruthParticleContainer* truthParticles);

  // process a single reconstructed track
  void processTrack(const xAOD::TruthParticle* truth, 
                    const xAOD::TrackParticle* reco, 
                    const xAOD::TrackParticle* ideal);

  // get track parameters
  void getParams(const xAOD::TruthParticle* truth, const xAOD::TrackParticle* track);

  // get hit information by comparing track to ideal track
  bool getHits(const xAOD::TrackParticle* ideal, const xAOD::TrackParticle* track);

  // fill track information
  void fillTrack(const xAOD::TruthParticle* truth, const xAOD::TrackParticle* track);

  void fillUnusedHits(const xAOD::TrackParticleContainer* ideal_tracks);

  // fill information for missing track hits
  void processMissingHit(const xAOD::TrackMeasurementValidation* cluster, std::string det);

  // fill information for missing track hits
  void processWrongHitSameLayer(const xAOD::TrackMeasurementValidation* idealClus, const xAOD::TrackMeasurementValidation* recoClus, std::string det);

  // get the origin of a truth particle
  std::string getParticleOrigin(const xAOD::TruthParticle* truthParticle);

  // get parents
  const xAOD::TruthParticle* getParent(const xAOD::TruthParticle* particle);
  const xAOD::TruthParticle* getParentBHadron(const xAOD::TruthParticle* particle);
  const xAOD::TruthParticle* getParentDHadron(const xAOD::TruthParticle* particle);

  // get track msos
  const xAOD::TrackStateValidation* getMSOS( MeasurementsOnTrackIter pointer );

  // get a string describing the detector a msos is on
  const std::string getDetString(const xAOD::TrackStateValidation* msos, 
                                 const xAOD::TrackMeasurementValidation* cluster);
}; 

#endif //> !TRACKANALYSIS_TRACKANALYSISALG_H
