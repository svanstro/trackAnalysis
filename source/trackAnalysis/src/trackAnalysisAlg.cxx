// trackAnalysis includes
#include "trackAnalysisAlg.h"




trackAnalysisAlg::trackAnalysisAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration
}

trackAnalysisAlg::~trackAnalysisAlg() {}

StatusCode trackAnalysisAlg::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //

  //example of retrieval of CutBookkeepers: (remember you will need to include the necessary header files and use statements in requirements file)
  // const xAOD::CutBookkeeperContainer* bks = 0;
  // CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );

  //example of IOVMetaData retrieval (see https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AthAnalysisBase#How_to_access_file_metadata_in_C)
  //float beamEnergy(0); CHECK( retrieveMetadata("/TagInfo","beam_energy",beamEnergy) );
  //std::vector<float> bunchPattern; CHECK( retrieveMetadata("/Digitiation/Parameters","BeamIntensityPattern",bunchPattern) );



  return StatusCode::SUCCESS;
}

StatusCode trackAnalysisAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

  //HERE IS AN EXAMPLE
  //We will create a histogram and a ttree and register them to the histsvc
  //Remember to configure the histsvc stream in the joboptions
  //
  //m_myHist = new TH1D("myHist","myHist",100,0,100);
  //CHECK( histSvc()->regHist("/MYSTREAM/myHist", m_myHist) ); //registers histogram to output stream
  //m_myTree = new TTree("myTree","myTree");
  //CHECK( histSvc()->regTree("/MYSTREAM/SubDirectory/myTree", m_myTree) ); //registers tree to output stream inside a sub-directory

  ATH_MSG_INFO("Initializing " << name() << "...");

  // ============================================
  // User config ================================
  // ============================================
  // set runnnig mode
  m_running_mode = "reco"; // options: "reco", "pseudo", "reco-pseudo", "candidates"
  m_fast_mode = true; // 2-3x speedup for fast mode
  // ============================================
  // ============================================
  
  ATH_MSG_INFO("Running mode = " << m_running_mode << "...");
  ATH_MSG_INFO("Fast mode enabled = " << m_fast_mode << "...");

  // specify track origin categories
  if ( !m_fast_mode ) {
    m_origins = {"All", "B", "D", "BHad", "DHad", "GEANT", "PU&UE", "Other"};
    m_hadrons = {"B", "D"}; // treat these differently because we might want to plot parent information
  }
  else {
    m_origins = {"All", "B"};
    m_hadrons = {"B"};
  }

  // set plotting style
  SetAtlasStyle();

  // book histograms
  ATH_MSG_INFO("Booking histograms...");
  CHECK( bookAllHistograms() );

  ATH_MSG_INFO("Initiliation complete.");
  return StatusCode::SUCCESS;
}

StatusCode trackAnalysisAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //

  ATH_MSG_INFO ("Finished processing " << eventCounter << " events and " << trackCounter << " tracks.");

  return StatusCode::SUCCESS;
}

StatusCode trackAnalysisAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed
  eventCounter++;
  
  if (eventCounter % 10000  == 0) {
    ATH_MSG_INFO("Processed "<< eventCounter << " events.");
  }

  // ---------------------------------------------------------------------
  // Retrieve data
  // ---------------------------------------------------------------------
  // event info
  //ei = nullptr;
  CHECK( evtStore()->retrieve( m_ei , "EventInfo" ) );
  hist("EventInfo/h_averagePileup")->Fill( m_ei->averageInteractionsPerCrossing() );

  // truth particles
  //truthParticles = nullptr;
  CHECK( evtStore()->retrieve( m_truthParticles , "TruthParticles" ) ); 
  //hist("EventInfo/h_nTruthParticles")->Fill( truthParticles->size() );

  // reconstructed tracks
  //reconstructedTracks = nullptr;
  CHECK( evtStore()->retrieve( m_reconstructedTracks , "InDetTrackParticles" ) ); //InDetTrackParticles
  //hist("EventInfo/h_nRecoTracks")->Fill( reconstructedTracks->size() );

  // pseudo-tracks
  //pseudotracks = nullptr;
  CHECK( evtStore()->retrieve( m_pseudotracks , "InDetPseudoTrackParticles" ) ); 
  //CHECK( evtStore()->retrieve( pseudotracks , "InDetTrackParticles" ) ); 

  if ( m_running_mode == "candidates" ) {
    CHECK( evtStore()->retrieve( m_candidates , "SiSpTrackCandidatesTrackParticle" ) );  
  }
  
  /* Old way to read cluster/truth information: may be useful later.
  // ---------------------------------------------------------------------
  // Get clusters
  // ---------------------------------------------------------------------
  // Pixel Clusters
  // get cluster containers of interest
  const xAOD::TrackMeasurementValidationContainer * pixClusters = nullptr;
  CHECK( evtStore()->retrieve( pixClusters , "PixelClusters" ) );
  if (not pixClusters) return StatusCode::FAILURE;
  
  //Make a map of all the pixel hits for each truth particle in the event
  std::multimap<int, const xAOD::TrackMeasurementValidation*> barcodePixClusterMap; // multimap is a map that permits multiple entries with the same key

  // for each cluster 
  for ( const auto * pixCluster : *pixClusters ) {
    
    std::vector<int> truthBarcodesInCluster;
    
    // if the cluster is availible
    if( pixCluster->isAvailable< std::vector<int> >( "truth_barcode" ) ) {

      // store the truth barcode
      truthBarcodesInCluster = pixCluster->auxdataConst< std::vector<int> >("truth_barcode");
      hist("Hits/nParticlesOnCluster/h_nTruthParticlesOnCluster")->Fill(truthBarcodesInCluster.size());
    }
    else {
      ATH_MSG_INFO("Truth barcodes on cluster not availible!");
    }
    

    // for each barcode in this cluster
    for( auto truthBarcode : truthBarcodesInCluster ) {

      // map the barcode to which cluster it came from
      barcodePixClusterMap.insert( std::make_pair( truthBarcode, pixCluster ) );
    }
  }

  // SCT Clusters
  // get cluster container of interest
  const xAOD::TrackMeasurementValidationContainer * sctClusters = nullptr;
  CHECK( evtStore()->retrieve( sctClusters , "SCT_Clusters" ) );
  if (not sctClusters) return StatusCode::FAILURE;

  //Make a map of all the SCT hits for each truth particle in the event
  std::multimap<int, const xAOD::TrackMeasurementValidation*> barcodeSCTClusterMap; // multimap is a map that permits multiple entries with the same key

  // for each cluster 
  for ( const auto * sctCluster : *sctClusters ) {
    
    std::vector<int> truthBarcodesInCluster;
    
    // if the cluster is availible
    if( sctCluster->isAvailable< std::vector<int> >( "truth_barcode" ) ) {

      // store the truth barcode
      truthBarcodesInCluster = sctCluster->auxdataConst< std::vector<int> >("truth_barcode");
    }

    // for each barcode in this cluster
    for( auto truthBarcode : truthBarcodesInCluster ) {

      // map the barcode to which cluster it came from
      barcodeSCTClusterMap.insert( std::make_pair( truthBarcode, sctCluster ) );
    }
  }
  // ---------------------------------------------------------------------
  */


  // do some initialisation
  auto all_tracks = setupTracks(m_truthParticles, m_reconstructedTracks, m_pseudotracks, m_running_mode);
  const xAOD::TrackParticleContainer* study_tracks = all_tracks.first; // tracks to study
  const xAOD::TrackParticleContainer* ideal_tracks = all_tracks.second; // ideal tracks to compare to

  auto m_truthParticleOriginMap = std::unordered_map<const xAOD::TruthParticle*, std::string>();

  // loop over the ideal tracks and mark linked truth particles as reconstructable
  mapTruthReconstructability(ideal_tracks);

  // loop again and process reconstructed tracks
  runRecoLoop(study_tracks);

  // loop over truth particles to fill efficiency plots
  runTruthLoop(m_truthParticles);  

  // expensive: turn off when not needed
  if ( !m_fast_mode ) {
    fillUnusedHits(m_pseudotracks);  
  }
  
  setFilterPassed(true); //if got here, assume that means algorithm passed
  return StatusCode::SUCCESS;
}


std::pair <const xAOD::TrackParticleContainer*,const xAOD::TrackParticleContainer*> 
trackAnalysisAlg::setupTracks( const xAOD::TruthParticleContainer* truthParticles,
                               const xAOD::TrackParticleContainer* reconstructedTracks,
                               const xAOD::TrackParticleContainer* pseudotracks,
                               std::string running_mode ) 
{ 
  // todo: read data here

  // standard mode: compare reconstruct tracks to ideal pseudo-tracks
  if ( running_mode == "reco" ) {
    return std::make_pair(reconstructedTracks, pseudotracks);
  } 
  // pseudo-track mode: get the ideal set of results
  else if ( running_mode == "pseudo" ) {
    return std::make_pair(pseudotracks, pseudotracks);
  }
  // reco - reco pseudo-track mode: get results for pseudo-tracks which have a 
  // corresponding reconstructed track. Compare to standard reco mode to see only
  // the effect of fixing hit assignment, but leaving efficiency unchanged
  else if ( running_mode == "reco-pseudo" ) {
    // result just has to be an iterable so can add the tracks to a vector and return
    // (though it will be the wrong type)
    //xAOD::TrackParticleContainer* outputTracks = new xAOD::TrackParticleContainer(); 

    //for ( auto track : *reconstructedTracks ) {
    //  outputTracks->push_back(track);
    //}
    return std::make_pair(pseudotracks, pseudotracks);
  }
  else if ( running_mode == "candidates" ) {

    m_reconstructedTracks = m_candidates;
    return std::make_pair(m_candidates, pseudotracks); 
  }
  // reco pseudo - pseudo-track mode: get results for pseudo-tracks which don't have
  // a corresponding reconstructed track. Can look at unreconstructed truth particles,
  // and also get a sense for how good the efficincy
  //else if ( running_mode = "unreco-pseudo" ) {
  //  return std::make_pair(pseudotracks, pseudotracks);
  //}

  // compare candidates - reco?
  return std::make_pair(reconstructedTracks, pseudotracks);
}


void trackAnalysisAlg::mapTruthReconstructability( const xAOD::TrackParticleContainer* idealTracks )
{
  // initialize map for counting pseudotracks
  auto trackCounts = std::unordered_map<std::string, int>();

  // initialise vector of reconstructable truth particles
  m_reconstructableTruth = std::vector<const xAOD::TruthParticle*>();

  // initialise map going from truth particles to ideal tracks
  m_truthIdealMap = std::unordered_map<const xAOD::TruthParticle*, const xAOD::TrackParticle*>();

  for ( const auto ideal_track : *idealTracks ) {
    
    // try to get the the linked truth particle
    const xAOD::TruthParticle* linkedTruthParticle = getTruth(ideal_track);

    if ( !linkedTruthParticle or ideal_track->pt() < 500 ) {
      // couldn't find a valid truth link or low pT
      continue;
    }

    // get the particles origin
    auto origin = getParticleOrigin(linkedTruthParticle);

    // keep count
    trackCounts[origin]++; trackCounts["All"]++;

    // ok, we found a truth particle for this ideal track, store that info
    m_reconstructableTruth.push_back(linkedTruthParticle);
    m_truthIdealMap[linkedTruthParticle] = ideal_track;
  }

  for (const std::string origin : m_origins) {
    auto o = "From_" + origin;
    hist("EventInfo/Counts/"+o+"/h_nPseudoTracks_"+o)->Fill(trackCounts[origin]);
  }
}

bool trackAnalysisAlg::pseudotrackIsReconstructed(const xAOD::TruthParticle* ideaTruth)
{
  for ( const auto track : *m_reconstructedTracks ) { 
    auto recoTruth = getTruth(track);

    if ( ideaTruth == recoTruth ) {
      return true;
    }
  }
  return false;
}

void trackAnalysisAlg::runRecoLoop(const xAOD::TrackParticleContainer* tracks)
{ 
  // initialise some stuff
  int nMissingTruthLinks = 0;
  auto trackCounts = std::unordered_map<std::string, int>();
  m_reconstructedTruth = std::vector<const xAOD::TruthParticle*>();
  m_clustersOnTracks = std::vector<const xAOD::TrackMeasurementValidation*>();
  
  // main track loop
  for ( const auto track : *tracks ) {
  
    // try to get the the linked truth particle
    const xAOD::TruthParticle* linkedTruthParticle = getTruth(track);
    m_origin = getParticleOrigin(linkedTruthParticle);

    // ignore low pt (pseudo) tracks
    if ( track->pt() < 500 ) {
      continue;
    }

    // if running mode is reco pseudo
    // continue if we cant find a reconstructed track linked to this truth particle
    if ( m_running_mode == "reco-pseudo" and !pseudotrackIsReconstructed(linkedTruthParticle) ) {
      continue;
    }

    // count the track
    trackCounts["All"]++; trackCounts[m_origin]++;

    // speedup (~30% faster)
    if ( m_fast_mode and m_origin != "B" ) {
      continue;
    }

    fill("EventInfo/TMP/<o>/p_truthMatchProbability_pT_<o>", track->pt()*0.001, track->auxdata<float>("truthMatchProbability"), m_origin);
    fill("EventInfo/TMP/<o>/p_fracGoodTracks_pT_<o>", track->pt()*0.001, int(track->auxdata<float>("truthMatchProbability")>0.75), m_origin);

    // continue if no valid truth link - probably a pileup track
    if ( !linkedTruthParticle ) {
      nMissingTruthLinks++;
      fill("Kinematics/MissingTruth/<o>/h_MissingTruth_pT_<o>", track->pt()*0.001); // GeV
      fill("Kinematics/MissingTruth/<o>/h_MissingTruth_eta_<o>", track->eta());
      fill("Kinematics/MissingTruth/<o>/h_MissingTruth_phi_<o>", track->phi());
      fill("Kinematics/MissingTruth/<o>/h_MissingTruth_d0_<o>", track->d0());
      fill("Kinematics/MissingTruth/<o>/h_MissingTruth_z0_<o>", track->z0());
      continue;
    }

    // check if we have a corresponding ideal track
    if ( m_truthIdealMap.find(linkedTruthParticle) == m_truthIdealMap.end() ) {
      // no corresponding ideal track. Suspect that this happens when the only
      // good hits on the track are in the TRT, and so no pseudo-track is made.
      // Then the track can pick up wrong hits in the Si. Happens ~3 tracks per
      // event. Can think of these as "fake Si" tracks and not worry too much.
    } else {
      // found an ideal track
      auto ideal_track = m_truthIdealMap[linkedTruthParticle];
      
      // mark the linked truth particle as having been reconstructed
      m_reconstructedTruth.push_back(linkedTruthParticle);
      
      //processTrack(linkedTruthParticle, track, ideal_track);
    }
  }

  // fill some per-event summary plots
  fill("EventInfo/Counts/<o>/h_fracTracksMissingTruthLink_<o>", float(nMissingTruthLinks) / float(tracks->size()));
  for (const std::string origin : m_origins) {
    auto o = "From_" + origin;
    hist("EventInfo/Counts/"+o+"/h_nRecoTracks_"+o)->Fill(trackCounts[origin]);
  }
}


void trackAnalysisAlg::runTruthLoop(const xAOD::TruthParticleContainer* truthParticles)
{ 
  auto truthCounts = std::unordered_map<std::string, int>();
  auto Binfo = std::unordered_map<const xAOD::TruthParticle*, std::unordered_map<std::string, int>>();
  auto Dinfo = std::unordered_map<const xAOD::TruthParticle*, std::unordered_map<std::string, int>>();

  for ( const auto truth : *truthParticles ) {
    
    // get truth particle origin
    auto origin = getParticleOrigin(truth);
    
    // speedup
    if ( m_fast_mode and m_origin != "B" ) {
      continue;
    }

    // count truth particles
    truthCounts["All"]++; truthCounts[origin]++;

    // fill truth dists
    fill("Kinematics/AllTruth/<o>/h_AllTruth_pT_<o>", truth->pt()*0.001, origin);
    fill("Kinematics/AllTruth/<o>/h_AllTruth_eta_<o>", truth->eta(), origin);
    fill("Kinematics/AllTruth/<o>/h_AllTruth_phi_<o>", truth->phi(), origin);
    fill("Kinematics/AllTruth/<o>/h_AllTruth_d0_<o>",  truth->auxdata<float>("d0"), origin);
    fill("Kinematics/AllTruth/<o>/h_AllTruth_z0_<o>",  truth->auxdata<float>("z0"), origin);
    if ( truth->auxdata<float>("z0") != 0 ) {
      fill("Kinematics/AllTruth/<o>/h_AllTruth_z0_nonzero_<o>",  truth->auxdata<float>("z0"), origin);
    }

    // do efficiency hists
    if ( std::find(m_reconstructableTruth.begin(), m_reconstructableTruth.end(), truth) != m_reconstructableTruth.end() ) {
      // the truth particle is reconstructable  

      auto o = "From_" + origin;
      
      double prod_rad = 0;
      if ( truth->hasProdVtx() ) { // check we have a production vertex otherwise we get a segmentation fault
        prod_rad = sqrt(pow(truth->prodVtx()->x(),2) + pow(truth->prodVtx()->y(),2));
      }
   
      if ( std::find(m_reconstructedTruth.begin(), m_reconstructedTruth.end(), truth) != m_reconstructedTruth.end() ) {
        // the particle was reconstructed
        fill("Efficiency/<o>/p_recoEfficiency_pT_<o>", truth->pt()*0.001, 1, origin);
        fill("Efficiency/<o>/p_recoEfficiency_PR_<o>", prod_rad, 1, origin);
        fill("Efficiency/<o>/p_recoEfficiency_d0_<o>", truth->auxdata<float>("d0"), 1, origin);

        if ( m_hadrons.find(origin) != m_hadrons.end() ) {
          auto parent = getParent(truth);
          if (parent) {
            double parent_pt = parent->pt()*0.001; // GeV
            float d_eta = truth->eta() - parent->eta();
            float d_phi = truth->phi() - parent->phi();
            if ( std::abs(d_phi) > TMath::Pi() ) {
              d_phi = std::abs(d_phi) - 2.0*TMath::Pi();
            }
            double parent_dR = TMath::Sqrt(pow(d_eta, 2) + pow(d_phi, 2));

            hist("Efficiency/"+o+"/p_recoEfficiency_pT"+origin+"_"+o)->Fill(parent_pt, 1);
            hist("Efficiency/"+o+"/p_recoEfficiency_dR_"+o)->Fill(parent_dR, 1);

            if ( origin == "B" ) {
              Binfo[parent]["truth"]++; Binfo[parent]["reco"]++;
            }
            else if ( origin == "D" ) {
              Dinfo[parent]["truth"]++; Dinfo[parent]["reco"]++;
            }
          }
        }
      }
      else {
        // the particle was not reconstructed
        fill("Efficiency/<o>/p_recoEfficiency_pT_<o>", truth->pt()*0.001, 0, origin);
        fill("Efficiency/<o>/p_recoEfficiency_PR_<o>", prod_rad, 0, origin);
        fill("Efficiency/<o>/p_recoEfficiency_d0_<o>", truth->auxdata<float>("d0"), 0, origin);

        if ( m_hadrons.find(origin) != m_hadrons.end() ) {
          auto parent = getParent(truth);
          if (parent) {
            double parent_pt = parent->pt()*0.001; // GeV

            float d_eta = truth->eta() - parent->eta();
            float d_phi = truth->phi() - parent->phi();
            if ( std::abs(d_phi) > TMath::Pi() ) {
              d_phi = std::abs(d_phi) - 2.0*TMath::Pi();
            }
            double parent_dR = TMath::Sqrt(pow(d_eta, 2) + pow(d_phi, 2));

            hist("Efficiency/"+o+"/p_recoEfficiency_pT"+origin+"_"+o)->Fill(parent_pt, 0);
            hist("Efficiency/"+o+"/p_recoEfficiency_dR_"+o)->Fill(parent_dR, 0);

            if ( origin == "B" ) {
              Binfo[parent]["truth"]++;
            }
            else if ( origin == "D" ) {
              Dinfo[parent]["truth"]++;
            }
          }
        }
      }
    }
  }

  // loop through hadrons and fill per hadron info
  for ( const auto B : Binfo ) {
    double B_DL = sqrt(pow(B.first->decayVtx()->x(),2) + pow(B.first->decayVtx()->y(),2));
    auto map = B.second;
    hist("Kinematics/HadronDecays/From_B/h_nTruthDecays_From_B")->Fill(map["truth"]);
    hist("Kinematics/HadronDecays/From_B/p_nTruthDecays_pT_From_B")->Fill(B.first->pt()*0.001, map["truth"]);
    hist("Kinematics/HadronDecays/From_B/p_nTruthDecays_DL_From_B")->Fill(B_DL, map["truth"]);
    hist("Kinematics/HadronDecays/From_B/p_nRecoTracks_pT_From_B")->Fill(B.first->pt()*0.001, map["reco"]);
    hist("Kinematics/HadronDecays/From_B/p_nRecoTracks_DL_From_B")->Fill(B_DL, map["reco"]);
  }
  for ( const auto D : Dinfo ) {
    double D_DL = sqrt(pow(D.first->decayVtx()->x(),2) + pow(D.first->decayVtx()->y(),2));
    auto map = D.second;
    hist("Kinematics/HadronDecays/From_D/h_nTruthDecays_From_D")->Fill(map["truth"]);
    hist("Kinematics/HadronDecays/From_D/p_nTruthDecays_pT_From_D")->Fill(D.first->pt()*0.001, map["truth"]);
    hist("Kinematics/HadronDecays/From_D/p_nTruthDecays_DL_From_D")->Fill(D_DL, map["truth"]);
    hist("Kinematics/HadronDecays/From_D/p_nRecoTracks_pT_From_D")->Fill(D.first->pt()*0.001, map["reco"]);
    hist("Kinematics/HadronDecays/From_D/p_nRecoTracks_DL_From_D")->Fill(D_DL, map["reco"]);
  }

  for ( const std::string origin : m_origins ) {
    auto o = "From_" + origin;
    hist("EventInfo/Counts/"+o+"/h_nTruthParticles_"+o)->Fill(truthCounts[origin]);
  }
}


void trackAnalysisAlg::processTrack(const xAOD::TruthParticle* truth, 
                                    const xAOD::TrackParticle* track, 
                                    const xAOD::TrackParticle* ideal)
{ 

  //auto origin = getParticleOrigin(truth);
  ATH_MSG_DEBUG("-- NEW TRACK with origin " << m_origin << std::endl);

  // get info
  getParams(truth, track);
  if ( !getHits(ideal, track) ) return; // don't fill if we can't get hit info

  // fill hists
  fillTrack(truth, track);
  trackCounter++;
}


bool trackAnalysisAlg::getHits(const xAOD::TrackParticle* ideal, const xAOD::TrackParticle* track)
{  
  // initialise some hit count maps
  m_total     = HitCounts();
  m_good      = HitCounts();
  m_wrong     = HitCounts();
  m_missing   = HitCounts();
  m_shared    = HitCounts();
  m_split     = HitCounts();
  m_outliers  = HitCounts();
  m_holes     = HitCounts();
  m_nTruthOnCluster = HitCounts();

  // this is an accessor: used to provide constant type-safe access to aux data
  static SG::AuxElement::ConstAccessor< MeasurementsOnTrack > acc_MeasurementsOnTrack(measurementNames);

  // get hits mesurements associated to the tracks
  const MeasurementsOnTrack& measurementsOnRecoTrack = acc_MeasurementsOnTrack(*track);
  auto reco_iter = measurementsOnRecoTrack.begin();

  const MeasurementsOnTrack& measurementsOnIdealTrack = acc_MeasurementsOnTrack(*ideal);
  auto ideal_iter = measurementsOnIdealTrack.begin();

  bool end_ideal = false;
  bool end_reco = false;

  // loop through measurements on both tracks
  while ( !(ideal_iter == measurementsOnIdealTrack.end() and reco_iter == measurementsOnRecoTrack.end()) ) {
    // handle tracks ending in different places
    if ( ideal_iter == measurementsOnIdealTrack.end() ) {
      ideal_iter--;
      end_ideal = true;
    }
    if ( reco_iter == measurementsOnRecoTrack.end() ) {
      reco_iter--;
      end_reco = true;
    }

    // get the MeaSurement On Surface
    auto msosIdeal = getMSOS(ideal_iter);
    auto msosReco = getMSOS(reco_iter);

    // check the measurement exists
    if ( msosIdeal == nullptr) {
      ATH_MSG_DEBUG("Problem reading cluster - may incorrectly process this ideal track with origin "<<m_origin);
      //ideal_iter++; // and then continue - is this the right thing to do?
      return false;
    }
    if ( msosReco == nullptr ) {
      ATH_MSG_DEBUG("Problem reading cluster - may incorrectly process this reco track with origin "<<m_origin);
      //reco_iter++; // and then continue - is this the right thing to do?
      return false;
    }
    
    // get the cluster associated to the measurement
    const xAOD::TrackMeasurementValidation* idealClus =  *(msosIdeal->trackMeasurementValidationLink());
    const xAOD::TrackMeasurementValidation* recoClus =  *(msosReco->trackMeasurementValidationLink());

    // mark the cluster as being used on a track
    m_clustersOnTracks.push_back(recoClus);

    // get the detector this cluster is on
    auto detString = getDetString(msosReco, recoClus);
    
    // record how many truth particles are on this cluster
    if ( !recoClus->isAvailable< std::vector<int> >( "truth_barcode" ) ) {
      m_nTruthOnCluster[detString] = -1;  
    }
    else {
      m_nTruthOnCluster[detString] = m_nTruthOnCluster[detString] + (recoClus->auxdataConst< std::vector<int> >("truth_barcode").size());  
    }

    //std::cout << "->HITINFO ideal: l=" << int(idealClus->auxdata<int>("layer") )<<" d="<<getDetString(msosIdeal, idealClus)<<" | reco: l=" << int(recoClus->auxdata<int>("layer") )<<" d="<<detString<<std::endl;

    // clasify the hit and iterate
    if ( idealClus->auxdata<int>("layer") == recoClus->auxdata<int>("layer") and 
         msosIdeal->detType() == msosReco->detType() and 
         idealClus->identifier() == recoClus->identifier() 
       ) {
      //std::cout << "good hit. reco: layer="<<recoClus->auxdata<int>("layer")<< " det=" <<int(msosReco->detType())<<"; ideal: layer="<<idealClus->auxdata<int>("layer")<< " det=" <<int(msosIdeal->detType()) << std::endl;
      // hit is good
      m_total[detString]++;
      m_good[detString]++;
      if ( detString == "IBL" or detString == "B" ) {
        m_total["Pix"]++; m_good["Pix"]++;
      }
      ideal_iter++; reco_iter++;
    }
    else if ( end_ideal ) { // catch end
      //std::cout<<"catch end ideal"<<std::endl;
      m_total[detString]++; m_wrong[detString]++;
      ideal_iter++; reco_iter++;
    }
    else if ( end_reco ) { // catch end
      //std::cout<<"catch end reco"<<std::endl;
      m_missing[detString]++;
      ideal_iter++; reco_iter++;
    }
    else if ( idealClus->auxdata<int>("layer") == recoClus->auxdata<int>("layer") and 
              msosIdeal->detType() == msosReco->detType() and
              idealClus->identifier() != recoClus->identifier()
            ) {
      //std::cout << "wrong same layer. reco: layer="<<recoClus->auxdata<int>("layer")<< " det=" <<int(msosReco->detType())<<"; ideal: layer="<<idealClus->auxdata<int>("layer")<< " det=" <<int(msosIdeal->detType()) << std::endl;
      // hit is wrong but on the same layer
      m_total[detString]++; m_wrong[detString]++;
      if ( detString == "IBL" or detString == "B" ) {
        m_total["Pix"]++; m_wrong["Pix"]++;
      }

      if ( detString != "SCT" and m_running_mode != "pseudo" and m_running_mode != "reco-pseudo" and
           idealClus->auxdata<int>("bec") == recoClus->auxdata<int>("bec") // somehow this sometimes comes out as 2 for one and 0 for the other...
         ) {
        // very occassionally (once in 100 test events), this will pass for pseudo-tracks unless
        // we catch it as above. Not clear why.
        processWrongHitSameLayer(idealClus, recoClus, detString);
      }

      ideal_iter++;
      reco_iter++;
    }
    else if ( ( idealClus->auxdata<int>("layer") > recoClus->auxdata<int>("layer") and
                msosIdeal->detType() == msosReco->detType() )
              or
               (msosIdeal->detType() == detSCT and msosReco->detType() == detPixel)
             ) {
      //std::cout << "wronghitdifflayer. reco: layer="<<recoClus->auxdata<int>("layer")<< " det=" <<int(msosReco->detType())<<"; ideal: layer="<<idealClus->auxdata<int>("layer")<< " det=" <<int(msosIdeal->detType()) << std::endl;
      // wrong hit is picled up with respect to ideal
      m_total[detString]++; m_wrong[detString]++;
      if ( detString == "IBL" or detString == "B" ) {
        m_total["Pix"]++; m_wrong["Pix"]++;
      }

      reco_iter++; // loop to catch up to ideal track
    } 
    else if ( ( idealClus->auxdata<int>("layer") < recoClus->auxdata<int>("layer") and
                msosIdeal->detType() == msosReco->detType() )
              or
                (msosIdeal->detType() == detPixel and msosReco->detType() == detSCT)
              ) {
      //std::cout << "missing hit. reco: layer="<<recoClus->auxdata<int>("layer")<< " det=" <<int(msosReco->detType())<<"; ideal: layer="<<idealClus->auxdata<int>("layer")<< " det=" <<int(msosIdeal->detType()) << std::endl;
      auto idealDetString = getDetString(msosIdeal, idealClus);
      m_missing[idealDetString]++;
      // hit is missing
      if ( idealDetString == "IBL" or idealDetString == "B" ) {
        m_missing["Pix"]++;
      }

      processMissingHit(idealClus, idealDetString);
      
      ideal_iter++; // loop to catch up to reco track
    }
    else {
      ATH_MSG_WARNING("Should not happen!");
    } 
  }


  // retrieve hit info from the aux data store
  int nHitsOnIBL         = track->auxdata<uint8_t>("numberOfInnermostPixelLayerHits");
  int nHitsOnB           = track->auxdata<uint8_t>("numberOfNextToInnermostPixelLayerHits");
  int nHitsOnPix         = track->auxdata<uint8_t>("numberOfPixelHits");
  int nHitsOnSCT         = track->auxdata<uint8_t>("numberOfSCTHits");

  // sanity checks
  if ( m_total["IBL"] != nHitsOnIBL ) {
    ATH_MSG_DEBUG("IBL mismatch, ignoring track.");
    return false;
  }
  if ( m_total["B"] != nHitsOnB ) {
    ATH_MSG_DEBUG("B mismatch, ignoring track.");
    return false;
  }
  if ( m_total["Pix"] != nHitsOnPix ) {
    ATH_MSG_DEBUG("Pix mismatch, ignoring track.");
    return false;
  }
  if ( m_total["SCT"] != nHitsOnSCT ) {
    ATH_MSG_DEBUG("SCT mismatch, ignoring track."); // this can happen very rarely
    return false;
  }
  if ( m_running_mode == "pseudo" and (m_wrong["Pix"] != 0 or  m_wrong["SCT"] != 0) ) {
    ATH_MSG_DEBUG("Wrong hit on pseudo-track, ignoring track."); // can happen to >0.1% of tracks
    return false;
  }

  // for some reason with the file PU reco_tf outiers and holes = 0
  m_shared["IBL"] = track->auxdata<uint8_t>("numberOfInnermostPixelLayerSharedHits");
  m_shared["B"]   = track->auxdata<uint8_t>("numberOfNextToInnermostPixelLayerSharedHits");
  m_shared["Pix"] = track->auxdata<uint8_t>("numberOfPixelSharedHits");
  m_shared["SCT"] = track->auxdata<uint8_t>("numberOfSCTSharedHits");
  m_split["IBL"] = track->auxdata<uint8_t>("numberOfInnermostPixelLayerSplitHits");
  m_split["B"]   = track->auxdata<uint8_t>("numberOfNextToInnermostPixelLayerSplitHits");
  m_split["Pix"] = track->auxdata<uint8_t>("numberOfPixelSplitHits");
  m_outliers["IBL"] = track->auxdata<uint8_t>("numberOfInnermostPixelLayerOutliers");
  m_outliers["B"]   = track->auxdata<uint8_t>("numberOfNextToInnermostPixelLayerOutliers");
  m_outliers["Pix"] = track->auxdata<uint8_t>("numberOfPixelOutliers");
  m_outliers["SCT"] = track->auxdata<uint8_t>("numberOfSCTOutliers");
  m_holes["Pix"] = track->auxdata<uint8_t>("numberOfPixelHoles");
  m_holes["SCT"] = track->auxdata<uint8_t>("numberOfSCTHoles");
  m_holes["SCT_double"] = track->auxdata<uint8_t>("numberOfSCTDoubleHoles");

  return true;
}

void trackAnalysisAlg::getParams(const xAOD::TruthParticle* truth, 
                                 const xAOD::TrackParticle* track)
{ 
  m_reco = {}; // clear
  m_reco.pt     = track->pt()*0.001; // GeV
  m_reco.eta    = track->eta();
  m_reco.theta  = track->theta();
  m_reco.phi    = track->phi();
  m_reco.d0     = track->d0();
  m_reco.z0     = track->z0();
  m_reco.qOverP = track->qOverP()*1000; // GeV

  m_truth = {}; // clear
  m_truth.pt     = truth->pt() * 0.001; // GeV
  m_truth.eta    = truth->eta();
  m_truth.theta  = truth->auxdata<float>("theta");
  m_truth.phi    = truth->phi();
  m_truth.d0     = truth->auxdata<float>("d0");
  m_truth.z0     = truth->auxdata<float>("z0");
  m_truth.qOverP = truth->charge() / (m_truth.pt * std::cosh(m_truth.eta));

  m_error = {}; // clear
  m_error.d0 = TMath::Sqrt(track->definingParametersCovMatrix()(0, 0));
  m_error.z0 = TMath::Sqrt(track->definingParametersCovMatrix()(1, 1));
  m_error.phi = TMath::Sqrt(track->definingParametersCovMatrix()(2, 2));
  m_error.theta = TMath::Sqrt(track->definingParametersCovMatrix()(3, 3));
  m_error.qOverP = TMath::Sqrt(track->definingParametersCovMatrix()(4, 4))*1000; // GeV

  m_prod_rad = 0;
  if ( truth->hasProdVtx() ) { // check we have a production vertex otherwise we get a segmentation fault
    m_prod_rad = sqrt(pow(truth->prodVtx()->x(),2) + pow(truth->prodVtx()->y(),2));
  }

  // failsafe (won't get filled as outside hist range)
  m_parent_pt = -1;
  m_parent_dR = -1;
  m_parent_DL = -1;

  if ( m_hadrons.find(m_origin) != m_hadrons.end() ) {
    auto parent = getParent(truth);
    if (parent) {
      m_parent_pt = parent->pt()*0.001; // GeV

      float d_eta = truth->eta() - parent->eta();
      float d_phi = truth->phi() - parent->phi();
      if ( std::abs(d_phi) > TMath::Pi() ) {
         d_phi = std::abs(d_phi) - 2.0*TMath::Pi();
      }
      m_parent_dR = TMath::Sqrt(pow(d_eta, 2) + pow(d_phi, 2));

      m_parent_DL = sqrt(pow(parent->decayVtx()->x(),2) + pow(parent->decayVtx()->y(),2));
    }
  }
}


void trackAnalysisAlg::processMissingHit(const xAOD::TrackMeasurementValidation* cluster, std::string det) 
{

  if ( det != "SCT" ) {
    //std::cout << "local X = " << cluster->localX() << std::endl;
    //std::cout << "global X = " << cluster->globalX() << std::endl;
    //double clusterRad   = sqrt( pow(cluster->globalX(),2) + pow(cluster->globalY(),2) + pow(cluster->globalZ(),2) );
    //double clusterTheta = std::atan2(cluster->globalY(),cluster->globalX());
    //double clusterPhi   = std::acos(cluster->globalZ()/clusterRad);

    //double d_theta = sqrt( pow(clusterTheta,2) + pow(m_truth.theta,2) );
    //double d_phi   = sqrt( pow(clusterPhi,2) + pow(m_truth.phi,2) );
    //double clus_dr = sqrt( pow(d_theta,2) + pow(d_phi,2) );

    int nTruthOnMissingCluster = 0;
    if ( !cluster->isAvailable<std::vector<int>>( "truth_barcode" ) ) {
      nTruthOnMissingCluster = -1;  
    }
    else {
      nTruthOnMissingCluster = cluster->auxdataConst< std::vector<int> >("truth_barcode").size();
    }

    fillHistogram(m_origin, "Hits/nOnCluster/<o>/Missing/p_nTruthOnMissing"+det+"Cluster_pT_<o>", nTruthOnMissingCluster);
    //fillHistogram(m_origin, "Hits/Res/TruthMissing/<o>/p_distToMissing"+det+"Cluster_pT_<o>", clus_dr);
  }
}

void trackAnalysisAlg::processWrongHitSameLayer(const xAOD::TrackMeasurementValidation* idealClus,
                                                const xAOD::TrackMeasurementValidation* recoClus,
                                                std::string det)
{
  double clus_dx = idealClus->globalX() - recoClus->globalX();
  double clus_dy = idealClus->globalY() - recoClus->globalY();
  double clus_dist = sqrt( pow(clus_dx,2) + pow(clus_dy,2) );
  //double clusterRad   = sqrt( pow(recoClus->globalX(),2) + pow(recoClus->globalY(),2) + pow(recoClus->globalZ(),2) );
  //double clusterTheta = std::abs(std::atan2(recoClus->globalY(),recoClus-globalZ()));
  //double clusterPhi   = std::acos(recoClus->globalZ()/clusterRad);
  //double clusterPhi = std::atan2(recoClus->globalY(),recoClus->globalX());
  //double d_theta = sqrt( pow(clusterTheta,2) + pow(m_reco.theta,2) );
  //double d_phi   = sqrt( pow(clusterPhi,2) + pow(m_reco.phi,2) );
  //double clus_dr = sqrt( pow(d_theta,2) + pow(d_phi,2) );

  fillHistogram(m_origin, "Hits/Res/GoodWrong/<o>/p_distToCorrect"+det+"Cluster_pT_<o>", clus_dist);
}


void trackAnalysisAlg::fillUnusedHits(const xAOD::TrackParticleContainer* ideal_tracks)
{
  // loop through pseudo-tracks
  for ( const auto ideal_track : *ideal_tracks ) {

    // ignore low pt (pseudo) tracks
    if ( ideal_track->pt() < 500 ) {
      continue;
    }

    // if we have a missing cluster we need to skip the track
    bool skip_track = false;

    // count hits
    auto counts = HitCounts();
    auto unused = HitCounts();

    // get truth info
    auto truth = getTruth(ideal_track);
    if ( !truth ) { // why is this happening!? Check some slimming
      ATH_MSG_WARNING ("Ideal Track truth information not available! Check slimming.");
      continue;
    }    
    auto origin = getParticleOrigin(truth);

    // get prod rad and pt
    double pt = ideal_track->pt()*0.001;
    double prod_rad = 0;
    if ( truth->hasProdVtx() ) { // check we have a production vertex otherwise we get a segmentation fault
      prod_rad = sqrt(pow(truth->prodVtx()->x(),2) + pow(truth->prodVtx()->y(),2));
    }

    // get parent info
    double parent_pt = -1;
    double parent_dR = -1;
    if ( m_hadrons.find(origin) != m_hadrons.end() ) {
      auto parent = getParent(truth);
      if (parent) {
        parent_pt = parent->pt()*0.001; // GeV
        float d_eta = truth->eta() - parent->eta();
        float d_phi = truth->phi() - parent->phi();
        if ( std::abs(d_phi) > TMath::Pi() ) {
          d_phi = std::abs(d_phi) - 2.0*TMath::Pi();
        }
        parent_dR = TMath::Sqrt(pow(d_eta, 2) + pow(d_phi, 2));
      }
    }

    // access msos
    static SG::AuxElement::ConstAccessor< MeasurementsOnTrack > acc_MeasurementsOnTrack(measurementNames);
    const MeasurementsOnTrack& measurementsOnIdealTrack = acc_MeasurementsOnTrack(*ideal_track);

    // loop through msos
    for ( auto ideal_msos_iter=measurementsOnIdealTrack.begin(); ideal_msos_iter!=measurementsOnIdealTrack.end(); ++ideal_msos_iter ) {

      // get msos
      const auto ideal_msos = getMSOS(ideal_msos_iter);
      if ( !ideal_msos ) {
        skip_track = true;
        break;
      }

      // get cluster
      const auto ideal_cluster = *(ideal_msos->trackMeasurementValidationLink());

      // check if on reco track
      bool clusterIsOnTrack = std::find(m_clustersOnTracks.begin(), m_clustersOnTracks.end(), ideal_cluster) != m_clustersOnTracks.end();
      
      // get det of ideal cluster
      auto det = getDetString(ideal_msos, ideal_cluster);

      // count used and unused clusters
      counts[det]++;
      if ( det == "IBL" or det == "B" ) {
        counts["Pix"]++;
      }
      if ( det == "Pix" ) {
        counts["Pix34"]++;
      }

      if ( !clusterIsOnTrack ) {
        unused[det]++;
        if ( det == "IBL" or det == "B" ) {
          unused["Pix"]++;
        }
        if ( det == "Pix" ) {
          unused["Pix34"]++;
        }
      }

      // fill per msos
      auto o = "From_" + origin;
      fillHistogram(origin, "Hits/Used/<o>/HitOnTrack/p_"+det+"HitUsedOnTrack_pT_<o>", double(clusterIsOnTrack), pt, prod_rad, parent_pt, parent_dR);
      if ( det == "IBL" or det == "B" ) {
        fillHistogram(origin, "Hits/Used/<o>/HitOnTrack/p_PixHitUsedOnTrack_pT_<o>", double(clusterIsOnTrack), pt, prod_rad, parent_pt, parent_dR);
      }
      if ( det == "Pix" ) { // exclusive 3,4 layer pix
        fillHistogram(origin, "Hits/Used/<o>/HitOnTrack/p_Pix34HitUsedOnTrack_pT_<o>", double(clusterIsOnTrack), pt, prod_rad, parent_pt, parent_dR);
      }
    }

    // don't fill tracks with missing msos
    if ( skip_track ) {
      continue;
    }

    // still have unused hit on pt

    // fill per track
    fillHistogram(origin, "Hits/Used/<o>/fracTrack/p_fracTracksWithUnusedSi_pT_<o>", unused["Pix"] != 0 and unused["SCT"] != 0, pt, prod_rad, parent_pt, parent_dR);
    fillHistogram(origin, "Hits/Used/<o>/fracTrack/p_fracTracksWith1UnusedPix3UnusedSCT_pT_<o>", unused["Pix"] != 0 and unused["SCT"] > 3, pt, prod_rad, parent_pt, parent_dR);
    fillHistogram(origin, "Hits/Used/<o>/fracTrack/p_fracTracksWith2UnusedPix6UnusedSCT_pT_<o>", unused["Pix"] > 1 and unused["SCT"] > 5, pt, prod_rad, parent_pt, parent_dR);

    if ( counts["Pix"] != 0 and unused["Pix"] != 0 and !skip_track ) {
      fillHistogram(origin, "Hits/Used/<o>/fracUnused/p_fracPixUnusedPerTrack_pT_<o>", unused["Pix"]/counts["Pix"], pt, prod_rad, parent_pt, parent_dR);
    }
    if ( counts["Pix34"] != 0 and unused["Pix34"] != 0 and !skip_track ) {
      fillHistogram(origin, "Hits/Used/<o>/fracUnused/p_fracPix34UnusedPerTrack_pT_<o>", unused["Pix34"]/counts["Pix34"], pt, prod_rad, parent_pt, parent_dR);
    }
    if ( counts["SCT"] != 0 and unused["SCT"] != 0 and !skip_track ) {
      fillHistogram(origin, "Hits/Used/<o>/fracUnused/p_fracSCTUnusedPerTrack_pT_<o>", unused["SCT"]/counts["SCT"], pt, prod_rad, parent_pt, parent_dR);
    }

    // if track is not reconstructed
    if ( std::find(m_reconstructedTruth.begin(), m_reconstructedTruth.end(), truth) == m_reconstructedTruth.end() ) {
      fillHistogram(origin, "Hits/Used/<o>/nUnusedPerUnreo/p_nPixUnusedPerUnreco_pT_<o>", unused["Pix"], pt, prod_rad, parent_pt, parent_dR);
      fillHistogram(origin, "Hits/Used/<o>/nUnusedPerUnreo/p_nSCTUnusedPerUnreco_pT_<o>", unused["SCT"], pt, prod_rad, parent_pt, parent_dR);
    }
  }
}


void trackAnalysisAlg::fillTrack(const xAOD::TruthParticle* truth, 
                                 const xAOD::TrackParticle* track)
{
  auto o = "From_" + m_origin;

  // Kinematics
  // ---------------------------------------------------------  
  fill("Kinematics/TMTruth/<o>/h_TMTruth_pT_<o>", m_truth.pt, m_origin); // GeV
  fill("Kinematics/TMTruth/<o>/h_TMTruth_eta_<o>",m_truth.eta, m_origin);
  fill("Kinematics/TMTruth/<o>/h_TMTruth_phi_<o>",m_truth.phi, m_origin);
  fill("Kinematics/TMTruth/<o>/h_TMTruth_d0_<o>", m_truth.d0, m_origin);
  fill("Kinematics/TMTruth/<o>/h_TMTruth_z0_<o>", m_truth.z0, m_origin);
  if ( m_truth.pt < 20.0 ) {
    fill("Kinematics/TMTruth/<o>/h_TMTruth_pT_low_<o>", m_truth.pt, m_origin);
  }

  fill("Kinematics/trackParams/<o>/h_recoTruthResidual_d0_<o>",  m_reco.d0 - m_truth.d0, m_origin);
  fill("Kinematics/trackParams/<o>/h_recoTruthResidual_z0_<o>",  m_reco.z0 - m_truth.z0, m_origin);
  fill("Kinematics/trackParams/<o>/h_recoTruthResidual_phi_<o>", m_reco.phi - m_truth.phi, m_origin);
  fill("Kinematics/trackParams/<o>/h_recoTruthResidual_theta_<o>", m_reco.theta - m_truth.theta, m_origin);
  fill("Kinematics/trackParams/<o>/h_recoTruthResidual_qOverP_<o>", m_reco.qOverP/m_truth.qOverP - 1, m_origin); // GeV

  fill("Kinematics/trackParams/<o>/h_recoTruthPull_d0_<o>",     (m_reco.d0 - m_truth.d0)   / m_error.d0, m_origin);
  fill("Kinematics/trackParams/<o>/h_recoTruthPull_z0_<o>",     (m_reco.z0 - m_truth.z0)   / m_error.z0, m_origin);
  fill("Kinematics/trackParams/<o>/h_recoTruthPull_phi_<o>",    (m_reco.phi - m_truth.phi) / m_error.phi, m_origin);
  fill("Kinematics/trackParams/<o>/h_recoTruthPull_theta_<o>",  (m_reco.theta - m_truth.theta)    / m_error.theta, m_origin);
  fill("Kinematics/trackParams/<o>/h_recoTruthPull_qOverP_<o>", (m_reco.qOverP - m_truth.qOverP)  / m_error.qOverP, m_origin); // GeV

  fill("Kinematics/d0/<o>/p_d0_size_pT_<o>", m_reco.pt, std::abs(m_reco.d0), m_origin);
  fill("Kinematics/d0/<o>/p_d0_error_pT_<o>", m_reco.pt, m_error.d0, m_origin);
  fill("Kinematics/d0/<o>/p_d0_sig_size_pT_<o>", m_reco.pt, std::abs(m_reco.d0)/m_error.d0, m_origin);
  fill("Kinematics/d0/<o>/p_d0_pull_size_pT_<o>", m_reco.pt, std::abs((m_reco.d0 - m_truth.d0) / m_error.d0), m_origin);

  if ( m_hadrons.find(m_origin) != m_hadrons.end() ) {
    hist("Kinematics/HadronDecays/"+o+"/p_pTvsDL_"+o)                  -> Fill(m_parent_pt, m_parent_DL);
    hist("Kinematics/HadronDecays/"+o+"/p_averageDecayParticle_pT_"+o) -> Fill(m_parent_pt, m_truth.pt);
    hist("Kinematics/HadronDecays/"+o+"/p_averageDecayParticle_PR_"+o) -> Fill(m_parent_pt, m_prod_rad);
  
    hist("Kinematics/d0/"+o+"/p_d0_sig_size_pT"+m_origin+"_"+o)->Fill(m_parent_pt, std::abs(m_reco.d0)/m_error.d0);
    hist("Kinematics/d0/"+o+"/p_d0_pull_size_pT"+m_origin+"_"+o)->Fill(m_parent_pt, std::abs((m_reco.d0 - m_truth.d0) / m_error.d0));

  }

  // ---------------------------------------------------------
  // Hits
  // ---------------------------------------------------------
  std::vector<std::string> toFill = {"All", m_origin};
  
  for ( auto det : m_dets ) {
    for ( auto t_origin : toFill ) {
      o = "From_" + t_origin;
      hist("Hits/Total/"   +o+"/p_nHits"       +det+"_pT_"+o)->Fill(m_reco.pt,   m_total[det]);
      hist("Hits/Total/"   +o+"/p_nHits"       +det+"_PR_"+o)->Fill(m_prod_rad,  m_total[det]);
      hist("Hits/Good/"    +o+"/p_nGoodHits"   +det+"_pT_"+o)->Fill(m_reco.pt,   m_good[det]);
      hist("Hits/Good/"    +o+"/p_nGoodHits"   +det+"_PR_"+o)->Fill(m_prod_rad,  m_good[det]);
      hist("Hits/Wrong/"   +o+"/p_nWrongHits"  +det+"_pT_"+o)->Fill(m_reco.pt,   m_wrong[det]);
      hist("Hits/Wrong/"   +o+"/p_nWrongHits"  +det+"_PR_"+o)->Fill(m_prod_rad,  m_wrong[det]);
      hist("Hits/Missing/" +o+"/p_nMissingHits"+det+"_pT_"+o)->Fill(m_reco.pt,   m_missing[det]);
      hist("Hits/Missing/" +o+"/p_nMissingHits"+det+"_PR_"+o)->Fill(m_prod_rad,  m_missing[det]);
      hist("Hits/Shared/"  +o+"/p_nSharedHits" +det+"_pT_"+o)->Fill(m_reco.pt, m_shared[det]);
      hist("Hits/Shared/"  +o+"/p_nSharedHits" +det+"_PR_"+o)->Fill(m_prod_rad, m_shared[det]);
      hist("Hits/Unique/"  +o+"/p_nUniqueHits" +det+"_pT_"+o)->Fill(m_reco.pt, m_total[det]-m_shared[det]);
      hist("Hits/Unique/"  +o+"/p_nUniqueHits" +det+"_PR_"+o)->Fill(m_prod_rad, m_total[det]-m_shared[det]);
      hist("Hits/Outliers/"+o+"/p_nOutliers"   +det+"_pT_"+o)->Fill(m_reco.pt, m_outliers[det]);
      hist("Hits/Outliers/"+o+"/p_nOutliers"   +det+"_PR_"+o)->Fill(m_prod_rad, m_outliers[det]);

      if ( det != "SCT" ) {
        hist("Hits/Split/"+o+"/p_nSplitHits"+det+"_pT_"+o)->Fill(m_reco.pt, m_split[det]);
        hist("Hits/Split/"+o+"/p_nSplitHits"+det+"_PR_"+o)->Fill(m_prod_rad, m_split[det]);
        hist("Hits/Shared+Split/"+o+"/p_nShared+SplitHits"+det+"_pT_"+o)->Fill(m_reco.pt, m_shared[det]+m_split[det]);
        hist("Hits/Shared+Split/"+o+"/p_nShared+SplitHits"+det+"_PR_"+o)->Fill(m_prod_rad, m_shared[det]+m_split[det]);

        if ( m_total[det] != 0 ) {
          hist("Hits/Purity/"+o+"/p_hitPurity"+det+"_pT_"+o)->Fill(m_reco.pt, m_good[det]/m_total[det]);
          hist("Hits/Purity/"+o+"/p_hitPurity"+det+"_PR_"+o)->Fill(m_prod_rad, m_good[det]/m_total[det]);
          hist("Hits/nOnCluster/"+o+"/h_nTruthOn"+det+"Cluster_"+o)->Fill(m_nTruthOnCluster[det] / m_total[det]);

          if ( m_good[det] == m_total[det] ) {
            hist("Hits/nOnCluster/"+o+"/h_nTruthOnGood"+det+"Cluster_"+o)->Fill(m_nTruthOnCluster[det] / m_total[det]);
            hist("Hits/nOnCluster/"+o+"/Good/p_nTruthOnGood"+det+"Cluster_pT_"+o)->Fill(m_reco.pt, m_nTruthOnCluster[det] / m_total[det]);
            hist("Hits/nOnCluster/"+o+"/Good/p_nTruthOnGood"+det+"Cluster_PR_"+o)->Fill(m_prod_rad, m_nTruthOnCluster[det] / m_total[det]);
          }
          if ( m_wrong[det] == m_total[det] ) {
            hist("Hits/nOnCluster/"+o+"/h_nTruthOnWrong"+det+"Cluster_"+o)->Fill(m_nTruthOnCluster[det] / m_total[det]);
            hist("Hits/nOnCluster/"+o+"/Wrong/p_nTruthOnWrong"+det+"Cluster_pT_"+o)->Fill(m_reco.pt, m_nTruthOnCluster[det] / m_total[det]);
            hist("Hits/nOnCluster/"+o+"/Wrong/p_nTruthOnWrong"+det+"Cluster_PR_"+o)->Fill(m_prod_rad, m_nTruthOnCluster[det] / m_total[det]);
          }
        }
      }
    }

    if ( m_hadrons.find(m_origin) != m_hadrons.end() ) {
      o = "From_" + m_origin;
      hist("Hits/Total/"+o+"/p_nHits"+det+"_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_total[det]);
      hist("Hits/Total/"+o+"/p_nHits"+det+"_dR_"+o)->Fill(m_parent_dR, m_total[det]);
      hist("Hits/Good/"+o+"/p_nGoodHits"+det+"_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_good[det]);
      hist("Hits/Good/"+o+"/p_nGoodHits"+det+"_dR_"+o)->Fill(m_parent_dR, m_good[det]);
      hist("Hits/Wrong/"+o+"/p_nWrongHits"+det+"_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_wrong[det]);
      hist("Hits/Wrong/"+o+"/p_nWrongHits"+det+"_dR_"+o)->Fill(m_parent_dR, m_wrong[det]);
      hist("Hits/Missing/"+o+"/p_nMissingHits"+det+"_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_missing[det]);
      hist("Hits/Missing/"+o+"/p_nMissingHits"+det+"_dR_"+o)->Fill(m_parent_dR, m_missing[det]);
      hist("Hits/Shared/"+o+"/p_nSharedHits"+det+"_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_shared[det]);
      hist("Hits/Shared/"+o+"/p_nSharedHits"+det+"_dR_"+o)->Fill(m_parent_dR, m_shared[det]);
      hist("Hits/Unique/"+o+"/p_nUniqueHits"+det+"_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_total[det]-m_shared[det]);
      hist("Hits/Unique/"+o+"/p_nUniqueHits"+det+"_dR_"+o)->Fill(m_parent_dR, m_total[det]-m_shared[det]);
      hist("Hits/Outliers/"+o+"/p_nOutliers"+det+"_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_outliers[det]);
      hist("Hits/Outliers/"+o+"/p_nOutliers"+det+"_dR_"+o)->Fill(m_parent_dR, m_outliers[det]);

      if ( det != "SCT" ) {
        hist("Hits/Split/"+o+"/p_nSplitHits"+det+"_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_split[det]);
        hist("Hits/Split/"+o+"/p_nSplitHits"+det+"_dR_"+o)->Fill(m_parent_dR, m_split[det]);
        hist("Hits/Shared+Split/"+o+"/p_nShared+SplitHits"+det+"_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_shared[det]+m_split[det]);
        hist("Hits/Shared+Split/"+o+"/p_nShared+SplitHits"+det+"_dR_"+o)->Fill(m_parent_dR, m_shared[det]+m_split[det]);

        if ( m_total[det] != 0 ) {
          if ( m_good[det] == m_total[det] ) {
            hist("Hits/nOnCluster/"+o+"/Good/p_nTruthOnGood"+det+"Cluster_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_nTruthOnCluster[det] / m_total[det]);
            hist("Hits/nOnCluster/"+o+"/Good/p_nTruthOnGood"+det+"Cluster_dR_"+o)->Fill(m_parent_dR, m_nTruthOnCluster[det] / m_total[det]);
          }
          if ( m_wrong[det] == m_total[det] ) {
            hist("Hits/nOnCluster/"+o+"/Wrong/p_nTruthOnWrong"+det+"Cluster_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_nTruthOnCluster[det] / m_total[det]);
            hist("Hits/nOnCluster/"+o+"/Wrong/p_nTruthOnWrong"+det+"Cluster_dR_"+o)->Fill(m_parent_dR, m_nTruthOnCluster[det] / m_total[det]);
          }
        }
      }

      if ( m_total[det] != 0 ) {
        hist("Hits/Purity/"+o+"/p_hitPurity"+det+"_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_good[det]/m_total[det]);
        hist("Hits/Purity/"+o+"/p_hitPurity"+det+"_dR_"+o)->Fill(m_parent_dR, m_good[det]/m_total[det]);
      }
    }
  }

  if ( m_total["IBL"] == 1 ) {
    if ( m_shared["IBL"] == 1 ) {
      fill("Hits/Shared/<o>/GoodWrong/p_nGoodSharedIBL_pT_<o>", m_reco.pt, m_good["IBL"], m_origin);
      fill("Hits/Shared/<o>/GoodWrong/p_nGoodSharedIBL_PR_<o>", m_prod_rad, m_good["IBL"], m_origin);
      fill("Hits/Shared/<o>/GoodWrong/p_nWrongSharedIBL_pT_<o>", m_reco.pt, m_wrong["IBL"], m_origin);
      fill("Hits/Shared/<o>/GoodWrong/p_nWrongSharedIBL_PR_<o>", m_prod_rad, m_wrong["IBL"], m_origin);
    }
    if ( m_split["IBL"] == 1 ) {
      fill("Hits/Split/<o>/GoodWrong/p_nGoodSplitIBL_pT_<o>", m_reco.pt, m_good["IBL"], m_origin);
      fill("Hits/Split/<o>/GoodWrong/p_nGoodSplitIBL_PR_<o>", m_prod_rad, m_good["IBL"], m_origin);
      fill("Hits/Split/<o>/GoodWrong/p_nWrongSplitIBL_pT_<o>", m_reco.pt, m_wrong["IBL"], m_origin);
      fill("Hits/Split/<o>/GoodWrong/p_nWrongSplitIBL_PR_<o>", m_prod_rad, m_wrong["IBL"], m_origin);
    }
    if ( m_shared["IBL"] == 1 or m_split["IBL"] == 1) {
      fill("Hits/Shared+Split/<o>/GoodWrong/p_nGoodShared+SplitIBL_pT_<o>", m_reco.pt, m_good["IBL"], m_origin);
      fill("Hits/Shared+Split/<o>/GoodWrong/p_nGoodShared+SplitIBL_PR_<o>", m_prod_rad, m_good["IBL"], m_origin);
      fill("Hits/Shared+Split/<o>/GoodWrong/p_nWrongShared+SplitIBL_pT_<o>", m_reco.pt, m_wrong["IBL"], m_origin);
      fill("Hits/Shared+Split/<o>/GoodWrong/p_nWrongShared+SplitIBL_PR_<o>", m_prod_rad, m_wrong["IBL"], m_origin);
    }
    if ( (m_total["IBL"] - m_shared["IBL"]) == 1 ) {
      fill("Hits/Unique/<o>/GoodWrong/p_nGoodUniqueIBL_pT_<o>", m_reco.pt, m_good["IBL"], m_origin);
      fill("Hits/Unique/<o>/GoodWrong/p_nGoodUniqueIBL_PR_<o>", m_prod_rad, m_good["IBL"], m_origin);
      fill("Hits/Unique/<o>/GoodWrong/p_nWrongUniqueIBL_pT_<o>", m_reco.pt, m_wrong["IBL"], m_origin);
      fill("Hits/Unique/<o>/GoodWrong/p_nWrongUniqueIBL_PR_<o>", m_prod_rad, m_wrong["IBL"], m_origin);
    }
    if ( m_outliers["IBL"] == 1 ) {
      fill("Hits/Outliers/<o>/GoodWrong/p_nGoodOutlierIBL_pT_<o>", m_reco.pt, m_good["IBL"], m_origin);
      fill("Hits/Outliers/<o>/GoodWrong/p_nGoodOutlierIBL_PR_<o>", m_prod_rad, m_good["IBL"], m_origin);
      fill("Hits/Outliers/<o>/GoodWrong/p_nWrongOutlierIBL_pT_<o>", m_reco.pt, m_wrong["IBL"], m_origin);
      fill("Hits/Outliers/<o>/GoodWrong/p_nWrongOutlierIBL_PR_<o>", m_prod_rad, m_wrong["IBL"], m_origin);
    }
    if ( m_hadrons.find(m_origin) != m_hadrons.end() ) {
      if ( m_shared["IBL"] == 1 ) {
        hist("Hits/Shared/"+o+"/GoodWrong/p_nGoodSharedIBL_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_good["IBL"]);
        hist("Hits/Shared/"+o+"/GoodWrong/p_nGoodSharedIBL_dR_"+o)->Fill(m_parent_dR, m_good["IBL"]);
        hist("Hits/Shared/"+o+"/GoodWrong/p_nWrongSharedIBL_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_wrong["IBL"]);
        hist("Hits/Shared/"+o+"/GoodWrong/p_nWrongSharedIBL_dR_"+o)->Fill(m_parent_dR, m_wrong["IBL"]);
      }
      if ( m_split["IBL"] == 1 ) {
        hist("Hits/Split/"+o+"/GoodWrong/p_nGoodSplitIBL_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_good["IBL"]);
        hist("Hits/Split/"+o+"/GoodWrong/p_nGoodSplitIBL_dR_"+o)->Fill(m_parent_dR, m_good["IBL"]);
        hist("Hits/Split/"+o+"/GoodWrong/p_nWrongSplitIBL_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_wrong["IBL"]);
        hist("Hits/Split/"+o+"/GoodWrong/p_nWrongSplitIBL_dR_"+o)->Fill(m_parent_dR, m_wrong["IBL"]);
      }
      if ( m_shared["IBL"] == 1 or m_split["IBL"] == 1) {
        hist("Hits/Shared+Split/"+o+"/GoodWrong/p_nGoodShared+SplitIBL_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_good["IBL"]);
        hist("Hits/Shared+Split/"+o+"/GoodWrong/p_nGoodShared+SplitIBL_dR_"+o)->Fill(m_parent_dR, m_good["IBL"]);
        hist("Hits/Shared+Split/"+o+"/GoodWrong/p_nWrongShared+SplitIBL_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_wrong["IBL"]);
        hist("Hits/Shared+Split/"+o+"/GoodWrong/p_nWrongShared+SplitIBL_dR_"+o)->Fill(m_parent_dR, m_wrong["IBL"]);
      }
      if ( (m_total["IBL"] - m_shared["IBL"]) == 1 ) {
        hist("Hits/Unique/"+o+"/GoodWrong/p_nGoodUniqueIBL_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_good["IBL"]);
        hist("Hits/Unique/"+o+"/GoodWrong/p_nGoodUniqueIBL_dR_"+o)->Fill(m_parent_dR, m_good["IBL"]);
        hist("Hits/Unique/"+o+"/GoodWrong/p_nWrongUniqueIBL_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_wrong["IBL"]);
        hist("Hits/Unique/"+o+"/GoodWrong/p_nWrongUniqueIBL_dR_"+o)->Fill(m_parent_dR, m_wrong["IBL"]);
      }
      if ( m_outliers["IBL"] == 1 ) {
        hist("Hits/Outliers/"+o+"/GoodWrong/p_nGoodOutlierIBL_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_good["IBL"]);
        hist("Hits/Outliers/"+o+"/GoodWrong/p_nGoodOutlierIBL_dR_"+o)->Fill(m_parent_dR, m_good["IBL"]);
        hist("Hits/Outliers/"+o+"/GoodWrong/p_nWrongOutlierIBL_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_wrong["IBL"]);
        hist("Hits/Outliers/"+o+"/GoodWrong/p_nWrongOutlierIBL_dR_"+o)->Fill(m_parent_dR, m_wrong["IBL"]);
      }
    }
  }

  // Look for specific combinations of pixel hits (relevant to some ambi cuts)  
  fillHistogram(m_origin, "Hits/Combi/<o>/SharedShared/p_nSharedIBLSharedB_pT_<o>", m_shared["IBL"] == 1 and m_shared["B"] == 1);
  if ( m_shared["IBL"] == 1 and m_shared["B"] == 1 ) {
    fillHistogram(m_origin, "Hits/Combi/<o>/SharedShared/p_nWrongSharedIBLWrongSharedB_pT_<o>", m_wrong["IBL"] == 1 and m_wrong["B"] == 1);
  }
  
  fillHistogram(m_origin, "Hits/Combi/<o>/SplitSplit/p_nSplitIBLSplitB_pT_<o>", m_split["IBL"] == 1 and m_split["B"] == 1);
  if ( m_split["IBL"] == 1 and m_split["B"] == 1 ) {
    fillHistogram(m_origin, "Hits/Combi/<o>/SplitSplit/p_nWrongSplitIBLWrongSplitB_pT_<o>", m_wrong["IBL"] == 1 and m_wrong["B"] == 1);
  }
  
  fillHistogram(m_origin, "Hits/Combi/<o>/SharedSplit/p_nSharedIBLSplitB_pT_<o>", m_shared["IBL"] == 1 and m_split["B"] == 1);
  if ( m_shared["IBL"] == 1 and m_split["B"] == 1 ) {
    fillHistogram(m_origin, "Hits/Combi/<o>/SharedSplit/p_nWrongSharedIBLWrongSplitB_pT_<o>", m_wrong["IBL"] == 1 and m_wrong["B"] == 1);
  }

  fillHistogram(m_origin, "Hits/Combi/<o>/SplitShared/p_nSplitIBLSharedB_pT_<o>", m_split["IBL"] == 1 and m_shared["B"] == 1);
  if ( m_split["IBL"] == 1 and m_shared["B"] == 1 ) {
    fillHistogram(m_origin, "Hits/Combi/<o>/SplitShared/p_nWrongSplitIBLWrongSharedB_pT_<o>", m_wrong["IBL"] == 1 and m_wrong["B"] == 1);
  }

  fillHistogram(m_origin, "Hits/Combi/<o>/SplitSplitSplit/p_nSplitIBLSplitBSplitPix_pT_<o>", m_split["IBL"] == 1 and m_split["B"] == 1 and m_split["Pix"] > 2);
  if ( m_split["IBL"] == 1 and m_split["B"] == 1 and m_split["Pix"] > 2 ) {
    fillHistogram(m_origin, "Hits/Combi/<o>/SplitSplitSplit/p_nWrongSplitIBLWrongSplitBSplitPix_pT_<o>", m_wrong["IBL"] == 1 and m_wrong["B"] == 1);
  }

  fillHistogram(m_origin, "Hits/Combi/<o>/SplitSplitShared/p_nSplitIBLSplitBSharedPix_pT_<o>", m_split["IBL"] == 1 and m_split["B"] == 1 and (m_shared["Pix"] > 0 and m_shared["IBL"] == 0 and m_shared["B"] == 0) );
  if ( m_split["IBL"] == 1 and m_split["B"] == 1 and (m_shared["Pix"] > 0 and m_shared["IBL"] == 0 and m_shared["B"] == 0) ) {
    fillHistogram(m_origin, "Hits/Combi/<o>/SplitSplitShared/p_nWrongSplitIBLWrongSplitBSharedPix_pT_<o>", m_wrong["IBL"] == 1 and m_wrong["B"] == 1);
  }

  fillHistogram(m_origin, "Hits/Combi/<o>/AllSplit/p_nAllSplitPix_pT_<o>", m_split["IBL"] == 1 and m_split["B"] == 1 and (m_split["Pix"] ==4 and m_total["Pix"] == 4) );
  if ( m_split["IBL"] == 1 and m_split["B"] == 1 and (m_split["Pix"] == 4 and m_total["Pix"] == 4) ) {
    fillHistogram(m_origin, "Hits/Combi/<o>/AllSplit/p_fracWrongAllSplitPix_pT_<o>", m_wrong["IBL"] == 1 and m_wrong["B"] == 1);
    fillHistogram(m_origin, "Hits/Combi/<o>/AllSplit/p_fracWrongIBLAllSplitPix_pT_<o>", m_wrong["IBL"] == 1);
  }

  // specifying wrong hits
  fillHistogram(m_origin, "Hits/Combi/<o>/Wrong/p_nWrongIBLWrongB_pT_<o>", m_wrong["IBL"] == 1 and m_wrong["B"] == 1);

  // fill holes
  o = "From_" + m_origin;
  hist("Hits/Holes/"+o+"/p_nHolesPix_pT_"+o)->Fill(m_reco.pt, m_holes["Pix"]);
  hist("Hits/Holes/"+o+"/p_nHolesPix_PR_"+o)->Fill(m_prod_rad, m_holes["Pix"]);
  hist("Hits/Holes/"+o+"/p_nHolesSCT_pT_"+o)->Fill(m_reco.pt, m_holes["SCT"]);
  hist("Hits/Holes/"+o+"/p_nHolesSCT_PR_"+o)->Fill(m_prod_rad, m_holes["SCT"]);
  hist("Hits/Holes/"+o+"/p_nDoubleHolesSCT_pT_"+o)->Fill(m_reco.pt, m_holes["SCT_double"]);
  hist("Hits/Holes/"+o+"/p_nDoubleHolesSCT_PR_"+o)->Fill(m_prod_rad, m_holes["SCT_double"]);
  if ( m_hadrons.find(m_origin) != m_hadrons.end() ) {
    hist("Hits/Holes/"+o+"/p_nHolesPix_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_holes["Pix"]);
    hist("Hits/Holes/"+o+"/p_nHolesPix_dR_"+o)->Fill(m_parent_dR,m_holes["Pix"]);
    hist("Hits/Holes/"+o+"/p_nHolesSCT_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_holes["SCT"]);
    hist("Hits/Holes/"+o+"/p_nHolesSCT_dR_"+o)->Fill(m_parent_dR,m_holes["SCT"]);
    hist("Hits/Holes/"+o+"/p_nDoubleHolesSCT_pT"+m_origin+"_"+o)->Fill(m_parent_pt, m_holes["SCT_double"]);
    hist("Hits/Holes/"+o+"/p_nDoubleHolesSCT_dR_"+o)->Fill(m_parent_dR,m_holes["SCT_double"]);
  }
  // ---------------------------------------------------------
  // Track fit
  // ---------------------------------------------------------
  double chiSquared = track->auxdata<float>("chiSquared");
  double nDoF       = track->auxdata<float>("numberDoF");
  if ( nDoF > 0 ) {
    fill("TrackFit/<o>/p_ChiSquared_pT_<o>", m_reco.pt, chiSquared, m_origin);
    fill("TrackFit/<o>/p_ChiSquared_PR_<o>", m_prod_rad, chiSquared, m_origin);
    fill("TrackFit/<o>/p_nDoF_pT_<o>", m_reco.pt, nDoF, m_origin);
    fill("TrackFit/<o>/p_nDoF_PR_<o>", m_prod_rad, nDoF, m_origin);
    fill("TrackFit/<o>/p_ChiSquaredpernDoF_pT_<o>", m_reco.pt, chiSquared/nDoF, m_origin);
    fill("TrackFit/<o>/p_ChiSquaredpernDoF_PR_<o>", m_prod_rad, chiSquared/nDoF, m_origin);

    if ( m_hadrons.find(m_origin) != m_hadrons.end() ) {
      hist("TrackFit/"+o+"/p_ChiSquared_pT"+m_origin+"_"+o) -> Fill(m_reco.pt, chiSquared);
      hist("TrackFit/"+o+"/p_ChiSquared_dR_"+o)             -> Fill(m_parent_dR, chiSquared);
      hist("TrackFit/"+o+"/p_nDoF_pT"+m_origin+"_"+o)       -> Fill(m_reco.pt, nDoF);
      hist("TrackFit/"+o+"/p_nDoF_dR_"+o)                   -> Fill(m_parent_dR, nDoF);
      hist("TrackFit/"+o+"/p_ChiSquaredpernDoF_pT"+m_origin+"_"+o) -> Fill(m_reco.pt, chiSquared/nDoF);
      hist("TrackFit/"+o+"/p_ChiSquaredpernDoF_dR_"+o)      -> Fill(m_parent_dR, chiSquared/nDoF);
    }
  }
}



const xAOD::TrackStateValidation* trackAnalysisAlg::getMSOS(MeasurementsOnTrackIter pointer) {
    // check if the element link is valid
    if ( !(*pointer).isValid() ) return nullptr;
    const xAOD::TrackStateValidation* msos = *(*pointer);
    if ( !msos->trackMeasurementValidationLink().isValid() ) return nullptr;
    if ( !(*(msos->trackMeasurementValidationLink())) ) return nullptr;
    if ( msos->type() != 0 ) { return nullptr; } // if it's not a hole or an outlier auxdata<float>("phi");
    return msos;
}

const std::string trackAnalysisAlg::getDetString(const xAOD::TrackStateValidation* msos, 
                                                 const xAOD::TrackMeasurementValidation* cluster) 
{
  if ( msos->detType() == detPixel ) { // pixels
    if ( cluster->auxdata<int>("layer") == 0 and cluster->auxdata<int>("bec") == 0 ) { // IBL
      return "IBL";
    }
    else if ( cluster->auxdata<int>("layer") == 1 and cluster->auxdata<int>("bec") == 0 ) { // B-Layer
      return "B";
    }
    else {
      return "Pix";  
    } 
  }
  else if ( msos->detType() == detSCT ) { // SCT
    return "SCT";
  }
  ATH_MSG_WARNING("Unknown detector");
  return "";
}



const xAOD::TruthParticle* trackAnalysisAlg::getTruth( const xAOD::TrackParticle* track ) {

    // create a pointer to a truth particle which will correspond to this track
    const xAOD::TruthParticle* linkedTruthParticle = nullptr;

    // if the track doesnt't have a valid truth link, skip to the next track
    // in practice, all tracks seem to have a truth link, but we need to also
    // check whether it's valid
    if ( !track->isAvailable<TruthLink>("truthParticleLink") ) { 
      ATH_MSG_DEBUG ("truthParticleLink is not available!");
      return nullptr;
    }

    // retrieve the link and check its validity
    // in this way, we only fill plots for tracks that are truth matched
    const TruthLink &link = track->auxdata<TruthLink>("truthParticleLink");

    // a missing or invalid link implies truth particle has been dropped from 
    // the truth record at some stage - probably it was from pilup which by
    // default we don't store truth information for
    if(!link or !link.isValid()) {
      ATH_MSG_DEBUG ("truthParticleLink is not valid!");
      return nullptr;
    }

    // seems safe to access and return the linked truth particle
    linkedTruthParticle = (*link);
    return linkedTruthParticle;
}

// ---------------------------------------------------------------------
// Functions for determination of truth particle origin
// ---------------------------------------------------------------------
const xAOD::TruthParticle* trackAnalysisAlg::getParent(const xAOD::TruthParticle* particle) {
  if ( particle == nullptr ) {
    return nullptr;
  }

  auto origin = getParticleOrigin(particle);

  if ( origin == "B" ) {
    return getParentBHadron(particle);
  } 
  else if ( origin == "D" ) {
    return getParentDHadron(particle);
  }
  else {
    return nullptr;
  }
}

const xAOD::TruthParticle* trackAnalysisAlg::getParentBHadron(const xAOD::TruthParticle* particle) {
  if ( particle == nullptr ) {
    return nullptr;
  }
  if ( particle->isBottomHadron() ) {
    return particle;
  }
  for(unsigned int p = 0; p < particle->nParents(); p++) {
    const xAOD::TruthParticle* parent = particle->parent(p);
    auto result = getParentBHadron(parent);
    if ( result!= nullptr ) {
      return result;
    }
  }
  return nullptr;
}

const xAOD::TruthParticle* trackAnalysisAlg::getParentDHadron(const xAOD::TruthParticle* particle) {
  if ( particle == nullptr ) {
    return nullptr;
  }
  if ( particle->isCharmHadron() ) {
    return particle;
  }
  for(unsigned int p = 0; p < particle->nParents(); p++) {
    const xAOD::TruthParticle* parent = particle->parent(p);
    auto result = getParentDHadron(parent);
    if ( result!= nullptr ) {
      return result;
    }
  }
  return nullptr;
}

bool isFromB(const xAOD::TruthParticle* particle) {
  if ( particle == nullptr ) {
    return false;
  }
  if ( particle->isBottomHadron() ) {
    return true;
  }
  for(unsigned int p = 0; p < particle->nParents(); p++) {
    const xAOD::TruthParticle* parent = particle->parent(p);
    if( isFromB(parent) ) {
      return true;
    }
  }
  return false;
}

bool isFromD(const xAOD::TruthParticle* particle) {
  if ( particle == nullptr ) {
    return false;
  }
  if( particle->isCharmHadron() ) {
    return true;
  }
  for(unsigned int p = 0; p < particle->nParents(); p++) {
    const xAOD::TruthParticle* parent = particle->parent(p);
    if( isFromD(parent) ) {
      return true;
    }
  }
  return false;
}

bool isNotFromPileup(const xAOD::TruthParticle* particle) {
  if ( particle == nullptr ) {
    return false;
  }
  // sample specific! Z' has a pdgid of 32
  if ( particle->pdgId() == 32 or particle->pdgId() == 33 ) {
    return true;
  }
  for (unsigned int p=0; p<particle->nParents(); p++) {
    const xAOD::TruthParticle* parent = particle->parent(p);
    if ( isNotFromPileup( parent ) ) {
      return true;
    }
  }
  return false;
}

std::string identifyParticle(const xAOD::TruthParticle* truthParticle) {
  
  if ( !isNotFromPileup(truthParticle) ) {
    return "PU&UE";
  }
  else if ( truthParticle->barcode() > 2e5 ) {
    return "GEANT";
  }  

  // ok, we have a primary particle
  else if (truthParticle->isBottomHadron()) {

    // of the B doesn't decay, it cannot decay to a C
    if ( !truthParticle->hasDecayVtx() ) return "Other";

    // get the decay vertex
    const xAOD::TruthVertex* decayVtx = truthParticle->decayVtx();

    // check if any decay product of the B is also B, if so return other
    for ( unsigned int i = 0; i < decayVtx->nOutgoingParticles(); i++ ) {
      const xAOD::TruthParticle* thisOutgoingParticle = decayVtx->outgoingParticle(i);
      if ( thisOutgoingParticle->isBottomHadron() ) return "Other";
    }
    // if we didn't already return, must have a weakly decaying B
    return "BHad";
  }
  else if (truthParticle->isCharmHadron()) {

    // of the B doesn't decay, it cannot decay to a C
    if ( !truthParticle->hasDecayVtx() ) return "Other";

    // get the decay vertex
    const xAOD::TruthVertex* decayVtx = truthParticle->decayVtx();

    // check if any decay product of the B is also B, if so return other
    for ( unsigned int i = 0; i < decayVtx->nOutgoingParticles(); i++ ) {
      const xAOD::TruthParticle* thisOutgoingParticle = decayVtx->outgoingParticle(i);
      if ( thisOutgoingParticle->isCharmHadron() ) return "Other";
    }
    // if we didn't already return, must have a weakly decaying D
    return "DHad";
  }
  else if ( isFromB(truthParticle) ) {// and !isFromD(&truthParticle) ) {
    return "B";
  }
  else if ( !isFromB(truthParticle) and isFromD(truthParticle) ) {
    return "D";
  }
  else {
    return "Other";
  }
}

std::string trackAnalysisAlg::getParticleOrigin(const xAOD::TruthParticle* truthParticle) {
  std::string origin = identifyParticle(truthParticle);
  if ( m_origins.find(origin) == m_origins.end() ) {
    return "All";
  }
  else {
    return origin;  
  }
  // tried to build a simple cache but apparantly this doesn't work...

  // check if already know the origin
  /*
  if ( m_truthParticleOriginMap.find(truthParticle) != m_truthParticleOriginMap.end() ) {
    return m_truthParticleOriginMap[truthParticle];
  } 
  else {
    auto origin = identifyParticle(truthParticle);
    m_truthParticleOriginMap[truthParticle] = origin; // cache the result
    return origin;
  }
  */
}
// ---------------------------------------------------------------------
// ---------------------------------------------------------------------


// -----------------------------------------------------------------------------
// Hist filling helper functions
// -----------------------------------------------------------------------------
const char* trackAnalysisAlg::replaceAll(std::string str, const std::string& from, const std::string& to) {
  if(from.empty())
    return str.c_str();
  size_t start_pos = 0;
  while((start_pos = str.find(from, start_pos)) != std::string::npos) {
    str.replace(start_pos, from.length(), to);
    start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
  }
  return str.c_str();
}

void trackAnalysisAlg::fill(std::string id, float x, std::string origin)
{
  hist(replaceAll(id, "<o>", "From_All"))->Fill(x);

  if ( origin != "All" and m_origins.find(origin) != m_origins.end() ) {
    hist(replaceAll(id, "<o>", "From_"+origin))->Fill(x);    
  }
}

void trackAnalysisAlg::fill(std::string id, float x, float y, std::string origin)
{
  hist(replaceAll(id, "<o>", "From_All"))->Fill(x, y);

  if ( origin != "All" and m_origins.find(origin) != m_origins.end() ) {
    hist(replaceAll(id, "<o>", "From_"+origin))->Fill(x, y);    
  }
}

StatusCode trackAnalysisAlg::bookHistogram(const std::string origin, const std::string id, const std::string title) {

  auto y_label = title;
  auto this_title = title + " From " + origin;

  // book track pT
  CHECK( book( TProfile(id.c_str(), title.c_str(), tpT_nbins, 0.0, tpT_max ) ) );
  hist(id.c_str())->GetXaxis()->SetTitle("Track p_{T} [GeV]"); 
  hist(id.c_str())->GetYaxis()->SetTitle(y_label.c_str());

  // book particle PR
  CHECK( book( TProfile(replaceAll(id, "pT", "PR"), replaceAll(this_title, "pT", "PR"), PR_nbins, 0.0, PR_max ) ) );
  hist(replaceAll(id, "pT", "PR"))->GetXaxis()->SetTitle("Stable Particle Production Radius [mm]"); 
  hist(replaceAll(id, "pT", "PR"))->GetYaxis()->SetTitle(y_label.c_str());

  // if origin is B or D, we can fill parent pT and dR plots
  if ( m_hadrons.find(origin) != m_hadrons.end() ) {

    // parent pT
    CHECK( book( TProfile(replaceAll(id, "pT", "pT"+origin), replaceAll(this_title, "pT", "pT"+origin), hpT_nbins, 0.0, hpT_max ) ) );
    hist(replaceAll(id, "pT", "pT"+origin))->GetXaxis()->SetTitle(("p_{T}^{"+origin+"} [GeV]").c_str()); 
    hist(replaceAll(id, "pT", "pT"+origin))->GetYaxis()->SetTitle(y_label.c_str());

    // dR to parent
    CHECK( book( TProfile(replaceAll(id, "pT", "dR"), replaceAll(this_title, "pT", "dR"), dR_nbins, 0.0, dR_max ) ) );
    hist(replaceAll(id, "pT", "dR"))->GetXaxis()->SetTitle(("Track dR to "+origin+" Hadron").c_str()); 
    hist(replaceAll(id, "pT", "dR"))->GetYaxis()->SetTitle(y_label.c_str());
  }
  return StatusCode::SUCCESS;
}

void trackAnalysisAlg::fillHistogram(const std::string origin, const std::string id, double y_val,
  double track_pt, double prod_rad, double parent_pt, double parent_dR) 
{
  if ( track_pt == -1.0 and prod_rad == -1.0 and parent_pt == -1.0 and parent_dR == -1.0 ) {
    track_pt = m_reco.pt;
    prod_rad = m_prod_rad;
    parent_pt = m_parent_pt;
    parent_dR = m_parent_dR;
  }
  auto o = "From_"+origin;
  hist(replaceAll(id, "<o>", "From_All"))->Fill(track_pt, y_val);
  hist(replaceAll(id, "<o>", o))->Fill(track_pt, y_val);
  hist(replaceAll(replaceAll(id, "<o>", "From_All"), "pT", "PR"))->Fill(prod_rad, y_val);
  hist(replaceAll(replaceAll(id, "<o>", o), "pT", "PR"))->Fill(prod_rad, y_val);

  // if origin is B or D, we can fill parent pT and dR plots
  if ( m_hadrons.find(origin) != m_hadrons.end() ) {
    hist(replaceAll(replaceAll(id, "<o>", o), "pT", "pT"+origin))->Fill(parent_pt, y_val);
    hist(replaceAll(replaceAll(id, "<o>", o), "pT", "dR"))->Fill(parent_dR, y_val);
  }
}


StatusCode trackAnalysisAlg::bookAllHistograms() {

  std::string id = "";

  // event level histograms
  id = "EventInfo/h_averagePileup";
  CHECK( book( TH1D(id.c_str(), "Number of interactions per bunch crossing", 15, 0.0, 100.0 ) ) );
  hist(id.c_str())->GetXaxis()->SetTitle("Number of pileup interactions"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");
  
  // histograms for tracks split according to track origin
  for (const std::string origin : m_origins) {
    auto o = "From_" + origin;

    id = "EventInfo/Counts/"+o+"/h_nTruthParticles_"+o;
    CHECK( book( TH1D(id.c_str(), "Num Truth Particles", 50, 0.0, 10000.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Truth Particles"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "EventInfo/Counts/"+o+"/h_nRecoTracks_"+o;
    CHECK( book( TH1D(id.c_str(), "Num Reco Tracks", 50, 0.0, 1500.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("InDet Reco Tracks"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "EventInfo/Counts/"+o+"/h_nPseudoTracks_"+o;
    CHECK( book( TH1D(id.c_str(), "Num Pseudo Tracks", 50, 0.0, 1500.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("InDet Pseudo-Tracks"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "EventInfo/Counts/"+o+"/h_fracTracksMissingTruthLink_"+o;
    CHECK( book( TH1D(id.c_str(), "Fraction of Tracks Without a Valid Truth Link", 50, 0.0, 1.2 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Fraction of Tracks Without a Valid Truth Link"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "EventInfo/TMP/"+o+"/p_truthMatchProbability_pT_"+o;
    CHECK( book( TProfile(id.c_str(), "Truth Match Probability of Tracks", 20, 0.0, 2000.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("p_{T}^{Track}"); hist(id.c_str())->GetYaxis()->SetTitle("Truth Match Probability");

    id = "EventInfo/TMP/"+o+"/p_fracGoodTracks_pT_"+o;
    CHECK( book( TProfile(id.c_str(), "Fraction of Tracks with TMP > 0.75", 20, 0.0, 2000.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("p_{T}^{Track}"); hist(id.c_str())->GetYaxis()->SetTitle("Fraction of Tracks with TMP > 0.75");

    id = "Kinematics/AllTruth/"+o+"/h_AllTruth_pT_"+o;
    CHECK( book( TH1D(id.c_str(), "pT of Truth Particles", 160, 0.0, 2000.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Truth p_{T} [GeV]"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/AllTruth/"+o+"/h_AllTruth_eta_"+o;
    CHECK( book( TH1D(id.c_str(), "#eta of All Truth Particles", 20, -2.5, 2.5 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Truth #eta"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/AllTruth/"+o+"/h_AllTruth_phi_"+o;
    CHECK( book( TH1D(id.c_str(), "#phi of All Truth Particles", 20, -3.15, 3.15 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Truth #phi"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/AllTruth/"+o+"/h_AllTruth_d0_"+o;
    CHECK( book( TH1D(id.c_str(), "d_{0} of All Truth Particles", 50, -5.0, 5.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Truth d_{0} [mm]"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/AllTruth/"+o+"/h_AllTruth_z0_"+o;
    CHECK( book( TH1D(id.c_str(), "z_{0} of All Truth Particles", 50, -60.0, 60.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Truth z_{0} [mm]"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/AllTruth/"+o+"/h_AllTruth_z0_nonzero_"+o;
    CHECK( book( TH1D(id.c_str(), "z_{0} of All Truth Particles if z_{0} #neq 0", 50, -60.0, 60.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Truth z_{0} [mm]"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/MissingTruth/"+o+"/h_MissingTruth_pT_"+o;
    CHECK( book( TH1D(id.c_str(), "pT of Tracks with Missing Truth Links", 20, 0.0, 500.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Track p_{T} [GeV]"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/MissingTruth/"+o+"/h_MissingTruth_eta_"+o;
    CHECK( book( TH1D(id.c_str(), "#eta of Tracks with Missing Truth Links", 20, -2.5, 2.5 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Track #eta"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/MissingTruth/"+o+"/h_MissingTruth_phi_"+o;
    CHECK( book( TH1D(id.c_str(), "#phi of Tracks with Missing Truth Links", 20, -3.15, 3.15 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Track #phi"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/MissingTruth/"+o+"/h_MissingTruth_d0_"+o;
    CHECK( book( TH1D(id.c_str(), "d_{0} of Tracks with Missing Truth Links", 50, -5.0, 5.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Track d_{0} [mm]"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/MissingTruth/"+o+"/h_MissingTruth_z0_"+o;
    CHECK( book( TH1D(id.c_str(), "z_{0} of Tracks with Missing Truth Links", 50, -60.0, 60.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Track z_{0} [mm]"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/TMTruth/"+o+"/h_TMTruth_pT_"+o;
    CHECK( book( TH1D(id.c_str(), "pT of Truth Matched Truth Particles with pT > 20 GeV", 160, 0.0, 500.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Truth p_{T} [GeV]"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/TMTruth/"+o+"/h_TMTruth_pT_low_"+o;
    CHECK( book( TH1D(id.c_str(), "pT of Truth Matched Truth Particles with pT < 20 GeV", 40, 0.0, 20.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Truth p_{T} [GeV]"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/TMTruth/"+o+"/h_TMTruth_eta_"+o;
    CHECK( book( TH1D(id.c_str(), "#eta of Truth Matched Truth Particles", 20, -2.5, 2.5 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Truth #eta"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/TMTruth/"+o+"/h_TMTruth_phi_"+o;
    CHECK( book( TH1D(id.c_str(), "#phi of Truth Matched Truth Particles", 20, -3.15, 3.15 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Truth #phi"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/TMTruth/"+o+"/h_TMTruth_d0_"+o;
    CHECK( book( TH1D(id.c_str(), "d_{0} of Truth Matched Truth Particles", 50, -5.0, 5.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Truth d_{0} [mm]"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/TMTruth/"+o+"/h_TMTruth_z0_"+o;
    CHECK( book( TH1D(id.c_str(), "z_{0} of Truth Matched Truth Particles", 50, -60.0, 60.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Truth z_{0} [mm]"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/trackParams/"+o+"/h_recoTruthResidual_d0_"+o;
    CHECK( book( TH1D(id.c_str(), "d0 residuals", 50, -0.1, 0.1 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(" d_{0}^{reco} - d_{0}^{true} [mm]"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/trackParams/"+o+"/h_recoTruthResidual_z0_"+o;
    CHECK( book( TH1D(id.c_str(), "z0 residuals", 50, -0.1, 0.1 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(" z_{0}^{reco} - z_{0}^{true} [mm]"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/trackParams/"+o+"/h_recoTruthResidual_phi_"+o;
    CHECK( book( TH1D(id.c_str(), "phi residuals", 50, -0.01, 0.01 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(" #phi^{reco} - #phi^{true}"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/trackParams/"+o+"/h_recoTruthResidual_theta_"+o;
    CHECK( book( TH1D(id.c_str(), "theta residuals", 50, -0.005, 0.005 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(" #theta^{reco} - #theta^{true}"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/trackParams/"+o+"/h_recoTruthResidual_qOverP_"+o;
    CHECK( book( TH1D(id.c_str(), "qOverP residuals", 50, -0.2, 0.2 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(" q/p^{reco}/q/p^{true}-1"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/trackParams/"+o+"/h_recoTruthPull_d0_"+o;
    CHECK( book( TH1D(id.c_str(), "d0 pull", 50, -5.0, 5.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(" (d_{0}^{reco} - d_{0}^{true})/#sigma(d_{0})"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/trackParams/"+o+"/h_recoTruthPull_z0_"+o;
    CHECK( book( TH1D(id.c_str(), "z0 pull", 50, -5.0, 5.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(" (z_{0}^{reco} - z_{0}^{true})/#sigma(z_{0})"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/trackParams/"+o+"/h_recoTruthPull_phi_"+o;
    CHECK( book( TH1D(id.c_str(), "phi pull", 50, -5.0, 5.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(" (phi^{reco} - #phi^{true})/#sigma(#phi)"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/trackParams/"+o+"/h_recoTruthPull_theta_"+o;
    CHECK( book( TH1D(id.c_str(), "theta pull", 50, -5.0, 5.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(" (theta^{reco} - #theta^{true})/#sigma(#theta)"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/trackParams/"+o+"/h_recoTruthPull_qOverP_"+o;
    CHECK( book( TH1D(id.c_str(), "qOverP pull", 50, -5.0, 5.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(" (q/p^{reco} - q/p^{true})/#sigma(q/p)"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Efficiency/"+o+"/p_recoEfficiency_pT_"+o;
    CHECK( book( TProfile(id.c_str(), ("Fraction of "+origin+" tracks that are reconstructed").c_str(), 20, 0.0, tpT_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Stable Particle p_{T} [GeV]"); hist(id.c_str())->GetYaxis()->SetTitle("Reconstruction Efficiency");

    id = "Efficiency/"+o+"/p_recoEfficiency_PR_"+o;
    CHECK( book( TProfile(id.c_str(), ("Fraction of "+origin+" tracks that are reconstructed").c_str(), PR_nbins, 0.0, PR_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Stable Particle Production Radius [mm]"); hist(id.c_str())->GetYaxis()->SetTitle("Reconstruction Efficiency");

    id = "Efficiency/"+o+"/p_recoEfficiency_d0_"+o;
    CHECK( book( TProfile(id.c_str(), ("Fraction of "+origin+" tracks that are reconstructed").c_str(), 10, 0.0, 10.0  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("Stable Particle d_{0} [mm]"); hist(id.c_str())->GetYaxis()->SetTitle("Reconstruction Efficiency");

    id = "Hits/nOnCluster/"+o+"/h_nTruthOnIBLCluster_"+o;
    CHECK( book( TH1D(id.c_str(), "n Particles on IBL Cluster", 20, 0.0, 10.0  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("n Particles on IBL Cluster"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Hits/nOnCluster/"+o+"/h_nTruthOnBCluster_"+o;
    CHECK( book( TH1D(id.c_str(), "n Particles on B Cluster", 20, 0.0, 10.0  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("n Particles on B Cluster"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Hits/nOnCluster/"+o+"/h_nTruthOnPixCluster_"+o;
    CHECK( book( TH1D(id.c_str(), "n Particles on Pix Cluster", 20, 0.0, 10.0  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("n Particles on Pix Cluster"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Hits/nOnCluster/"+o+"/h_nTruthOnGoodIBLCluster_"+o;
    CHECK( book( TH1D(id.c_str(), "n Particles on Good IBL Cluster", 20, 0.0, 10.0  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("n Particles on Good IBL Cluster"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Hits/nOnCluster/"+o+"/h_nTruthOnGoodBCluster_"+o;
    CHECK( book( TH1D(id.c_str(), "n Particles on Good B Cluster", 20, 0.0, 10.0  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("n Particles on Good B Cluster"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Hits/nOnCluster/"+o+"/h_nTruthOnGoodPixCluster_"+o;
    CHECK( book( TH1D(id.c_str(), "n Particles on Pix Good Cluster", 20, 0.0, 10.0  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("n Particles on Good Pix Cluster"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Hits/nOnCluster/"+o+"/h_nTruthOnWrongIBLCluster_"+o;
    CHECK( book( TH1D(id.c_str(), "n Particles on Wrong IBL Cluster", 20, 0.0, 10.0  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("n Particles on Wrong IBL Cluster"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Hits/nOnCluster/"+o+"/h_nTruthOnWrongBCluster_"+o;
    CHECK( book( TH1D(id.c_str(), "n Particles on Wrong B Cluster", 20, 0.0, 10.0  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("n Particles on Wrong B Cluster"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Hits/nOnCluster/"+o+"/h_nTruthOnWrongPixCluster_"+o;
    CHECK( book( TH1D(id.c_str(), "n Particles on Pix Wrong Cluster", 20, 0.0, 10.0  ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle("n Particles on Wrong Pix Cluster"); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "TrackFit/"+o+"/p_ChiSquared_pT_"+o;
    CHECK( book( TProfile(id.c_str(), ("#chi^{2} of track "+o).c_str(), tpT_nbins, 0.0, tpT_max ) ) );
    hist(id)->GetXaxis()->SetTitle("Track p_{T} [GeV]"); hist(id)->GetYaxis()->SetTitle("#chi^{2}");

    id = "TrackFit/"+o+"/p_ChiSquared_PR_"+o;
    CHECK( book( TProfile(id.c_str(), ("#chi^{2} of track "+o).c_str(), PR_nbins, 0.0, PR_max ) ) );
    hist(id)->GetXaxis()->SetTitle("Stable Particle Production Radius [mm]"); hist(id)->GetYaxis()->SetTitle("#chi^{2}");

    id = "TrackFit/"+o+"/p_nDoF_pT_"+o;
    CHECK( book( TProfile(id.c_str(), ("nDoF of track "+o).c_str(), tpT_nbins, 0.0, tpT_max ) ) );
    hist(id)->GetXaxis()->SetTitle("Track p_{T} [GeV]"); hist(id)->GetYaxis()->SetTitle("Degrees of Freedom on Track");

    id = "TrackFit/"+o+"/p_nDoF_PR_"+o;
    CHECK( book( TProfile(id.c_str(), ("nDoF of track "+o).c_str(), PR_nbins, 0.0, PR_max ) ) );
    hist(id)->GetXaxis()->SetTitle("Stable Particle Production Radius [mm]"); hist(id)->GetYaxis()->SetTitle("Degrees of Freedom on Track");

    id = "TrackFit/"+o+"/p_ChiSquaredpernDoF_pT_"+o;
    CHECK( book( TProfile(id.c_str(), ("#chi^2 per NDoF "+o).c_str(), tpT_nbins, 0.0, tpT_max ) ) );
    hist(id)->GetXaxis()->SetTitle("Track p_{T} [GeV]"); hist(id)->GetYaxis()->SetTitle("#chi^{2}/n");

    id = "TrackFit/"+o+"/p_ChiSquaredpernDoF_PR_"+o;
    CHECK( book( TProfile(id.c_str(), ("#chi^2 per NDoF "+o).c_str(), PR_nbins, 0.0, PR_max ) ) );
    hist(id)->GetXaxis()->SetTitle("Stable Particle Production Radius [mm]"); hist(id)->GetYaxis()->SetTitle("#chi^{2}/n");

    id = "Kinematics/d0/"+o+"/p_d0_size_pT_"+o;
    CHECK( book( TProfile(id.c_str(), ("Track d0 size"+o).c_str(), tpT_nbins, 0.0, tpT_max ) ) );
    hist(id)->GetXaxis()->SetTitle("Track p_{T} [GeV]"); hist(id)->GetYaxis()->SetTitle("|d_{0}|");

    id = "Kinematics/d0/"+o+"/p_d0_error_pT_"+o;
    CHECK( book( TProfile(id.c_str(), ("Track d0 error"+o).c_str(), tpT_nbins, 0.0, tpT_max ) ) );
    hist(id)->GetXaxis()->SetTitle("Track p_{T} [GeV]"); hist(id)->GetYaxis()->SetTitle("#sigma(d_{0})");

    id = "Kinematics/d0/"+o+"/p_d0_sig_size_pT_"+o;
    CHECK( book( TProfile(id.c_str(), ("Track d0 sig size"+o).c_str(), tpT_nbins, 0.0, tpT_max ) ) );
    hist(id)->GetXaxis()->SetTitle("Track p_{T} [GeV]"); hist(id)->GetYaxis()->SetTitle("|d_{0}|/#sigma(d_{0})");

    id = "Kinematics/d0/"+o+"/p_d0_pull_size_pT_"+o;
    CHECK( book( TProfile(id.c_str(), ("Track d0 pull size"+o).c_str(), tpT_nbins, 0.0, tpT_max ) ) );
    hist(id)->GetXaxis()->SetTitle("Track p_{T} [GeV]"); hist(id)->GetYaxis()->SetTitle("d_{0} pull size");


    // book hit level hists
    CHECK ( bookHistogram(origin, "Hits/Total/"+o+"/p_nHitsIBL_pT_"+o,          "IBL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Total/"+o+"/p_nHitsB_pT_"+o,           "BL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Total/"+o+"/p_nHitsPix_pT_"+o,        "Pixel Hits") );
    CHECK ( bookHistogram(origin, "Hits/Total/"+o+"/p_nHitsSCT_pT_"+o,          "SCT Hits") );
    CHECK ( bookHistogram(origin, "Hits/Shared/"+o+"/p_nSharedHitsIBL_pT_"+o,   "Shared IBL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Shared/"+o+"/p_nSharedHitsB_pT_"+o,    "Shared BL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Shared/"+o+"/p_nSharedHitsPix_pT_"+o, "Shared Pixel Hits") );
    CHECK ( bookHistogram(origin, "Hits/Shared/"+o+"/p_nSharedHitsSCT_pT_"+o,   "Shared SCT Hits") );
    CHECK ( bookHistogram(origin, "Hits/Shared/"+o+"/GoodWrong/p_nGoodSharedIBL_pT_"+o,   "Fraction of Shared IBL Hits that are Good") );
    CHECK ( bookHistogram(origin, "Hits/Shared/"+o+"/GoodWrong/p_nWrongSharedIBL_pT_"+o,  "Fraction of Shared IBL Hits that are Wrong") );
    CHECK ( bookHistogram(origin, "Hits/Split/"+o+"/p_nSplitHitsIBL_pT_"+o,     "Split IBL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Split/"+o+"/p_nSplitHitsB_pT_"+o,      "Split BL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Split/"+o+"/p_nSplitHitsPix_pT_"+o,   "Split Pixel Hits") );
    CHECK ( bookHistogram(origin, "Hits/Split/"+o+"/GoodWrong/p_nGoodSplitIBL_pT_"+o,     "Fraction of Split IBL Hits that are Good") );
    CHECK ( bookHistogram(origin, "Hits/Split/"+o+"/GoodWrong/p_nWrongSplitIBL_pT_"+o,    "Fraction of Split IBL Hits that are Wrong") );
    CHECK ( bookHistogram(origin, "Hits/Shared+Split/"+o+"/p_nShared+SplitHitsIBL_pT_"+o,   "Split IBL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Shared+Split/"+o+"/p_nShared+SplitHitsB_pT_"+o,    "Split BL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Shared+Split/"+o+"/p_nShared+SplitHitsPix_pT_"+o, "Split Pixel Hits") );
    CHECK ( bookHistogram(origin, "Hits/Shared+Split/"+o+"/GoodWrong/p_nGoodShared+SplitIBL_pT_"+o,  "Fraction of Shared+Split IBL Good") );
    CHECK ( bookHistogram(origin, "Hits/Shared+Split/"+o+"/GoodWrong/p_nWrongShared+SplitIBL_pT_"+o, "Fraction of Shared+Split IBL Wrong") );
    CHECK ( bookHistogram(origin, "Hits/Unique/"+o+"/p_nUniqueHitsIBL_pT_"+o,   "Unique IBL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Unique/"+o+"/p_nUniqueHitsB_pT_"+o,    "Unique BL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Unique/"+o+"/p_nUniqueHitsPix_pT_"+o, "Unique Pixel Hits") );
    CHECK ( bookHistogram(origin, "Hits/Unique/"+o+"/p_nUniqueHitsSCT_pT_"+o,   "Unique SCT Hits") );
    CHECK ( bookHistogram(origin, "Hits/Unique/"+o+"/GoodWrong/p_nGoodUniqueIBL_pT_"+o,   "Fraction of Unique IBL Hits that are Good") );
    CHECK ( bookHistogram(origin, "Hits/Unique/"+o+"/GoodWrong/p_nWrongUniqueIBL_pT_"+o,  "Fraction of Unique IBL Hits that are Wrong") );
    CHECK ( bookHistogram(origin, "Hits/Outliers/"+o+"/p_nOutliersIBL_pT_"+o,   "Outlier IBL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Outliers/"+o+"/p_nOutliersB_pT_"+o,    "Outlier BL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Outliers/"+o+"/p_nOutliersPix_pT_"+o, "Outlier Pixel Hits") );
    CHECK ( bookHistogram(origin, "Hits/Outliers/"+o+"/p_nOutliersSCT_pT_"+o,   "Outlier SCT Hits") );
    CHECK ( bookHistogram(origin, "Hits/Outliers/"+o+"/GoodWrong/p_nGoodOutlierIBL_pT_"+o,  "Fraction of Outlier IBL Hits that are Good") );
    CHECK ( bookHistogram(origin, "Hits/Outliers/"+o+"/GoodWrong/p_nWrongOutlierIBL_pT_"+o, "Fraction of Outlier IBL Hits that are Wrong") );
    CHECK ( bookHistogram(origin, "Hits/Holes/"+o+"/p_nHolesPix_pT_"+o,       "Pixel Holes") );
    CHECK ( bookHistogram(origin, "Hits/Holes/"+o+"/p_nHolesSCT_pT_"+o,         "SCT Holes") );
    CHECK ( bookHistogram(origin, "Hits/Holes/"+o+"/p_nDoubleHolesSCT_pT_"+o,   "SCT Double Holes") );
    CHECK ( bookHistogram(origin, "Hits/Good/"+o+"/p_nGoodHitsIBL_pT_"+o,   "Good IBL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Good/"+o+"/p_nGoodHitsB_pT_"+o,    "Good BL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Good/"+o+"/p_nGoodHitsPix_pT_"+o,   "Good Pix Hits") );
    CHECK ( bookHistogram(origin, "Hits/Good/"+o+"/p_nGoodHitsSCT_pT_"+o,   "Good SCT Hits") );
    CHECK ( bookHistogram(origin, "Hits/Wrong/"+o+"/p_nWrongHitsIBL_pT_"+o, "Wrong IBL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Wrong/"+o+"/p_nWrongHitsB_pT_"+o,  "Wrong BL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Wrong/"+o+"/p_nWrongHitsPix_pT_"+o, "Wrong Pix Hits") );
    CHECK ( bookHistogram(origin, "Hits/Wrong/"+o+"/p_nWrongHitsSCT_pT_"+o, "Wrong SCT Hits") );
    CHECK ( bookHistogram(origin, "Hits/Missing/"+o+"/p_nMissingHitsIBL_pT_"+o, "Missing IBL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Missing/"+o+"/p_nMissingHitsB_pT_"+o,  "Missing BL Hits") );
    CHECK ( bookHistogram(origin, "Hits/Missing/"+o+"/p_nMissingHitsPix_pT_"+o, "Missing Pix Hits") );
    CHECK ( bookHistogram(origin, "Hits/Missing/"+o+"/p_nMissingHitsSCT_pT_"+o, "Missing SCT Hits") );
    CHECK ( bookHistogram(origin, "Hits/Purity/"+o+"/p_hitPurityIBL_pT_"+o, "IBL Good Hit Ratio") );
    CHECK ( bookHistogram(origin, "Hits/Purity/"+o+"/p_hitPurityB_pT_"+o,  "BL Good Hit Ratio") );
    CHECK ( bookHistogram(origin, "Hits/Purity/"+o+"/p_hitPurityPix_pT_"+o, "Pix Good Hit Ratio") );
    CHECK ( bookHistogram(origin, "Hits/Purity/"+o+"/p_hitPuritySCT_pT_"+o, "SCT Good Hit Ratio") );
    CHECK ( bookHistogram(origin, "Hits/nOnCluster/"+o+"/Good/p_nTruthOnGoodIBLCluster_pT_"+o,  "n Particles on Good IBL Cluster") );
    CHECK ( bookHistogram(origin, "Hits/nOnCluster/"+o+"/Good/p_nTruthOnGoodBCluster_pT_"+o,    "n Particles on Good B Cluster") );
    CHECK ( bookHistogram(origin, "Hits/nOnCluster/"+o+"/Good/p_nTruthOnGoodPixCluster_pT_"+o,  "n Particles on Good Pix Cluster") );
    CHECK ( bookHistogram(origin, "Hits/nOnCluster/"+o+"/Wrong/p_nTruthOnWrongIBLCluster_pT_"+o, "n Particles on Wrong IBL Cluster") );
    CHECK ( bookHistogram(origin, "Hits/nOnCluster/"+o+"/Wrong/p_nTruthOnWrongBCluster_pT_"+o,   "n Particles on Wrong B Cluster") );
    CHECK ( bookHistogram(origin, "Hits/nOnCluster/"+o+"/Wrong/p_nTruthOnWrongPixCluster_pT_"+o, "n Particles on Wrong Pix Cluster") );
    CHECK ( bookHistogram(origin, "Hits/nOnCluster/"+o+"/Missing/p_nTruthOnMissingIBLCluster_pT_"+o, "n Particles on Missing IBL Cluster") );
    CHECK ( bookHistogram(origin, "Hits/nOnCluster/"+o+"/Missing/p_nTruthOnMissingBCluster_pT_"+o,   "n Particles on Missing B Cluster") );
    CHECK ( bookHistogram(origin, "Hits/nOnCluster/"+o+"/Missing/p_nTruthOnMissingPixCluster_pT_"+o, "n Particles on Missing Pix Cluster") );
    CHECK ( bookHistogram(origin, "Hits/Combi/"+o+"/SharedShared/p_nSharedIBLSharedB_pT_"+o, "Shared IBL Shared B") );
    CHECK ( bookHistogram(origin, "Hits/Combi/"+o+"/SharedShared/p_nWrongSharedIBLWrongSharedB_pT_"+o, "Frac Wrong Shared IBL Wrong Shared B") );
    CHECK ( bookHistogram(origin, "Hits/Combi/"+o+"/SplitSplit/p_nSplitIBLSplitB_pT_"+o, "Split IBL Split B") );
    CHECK ( bookHistogram(origin, "Hits/Combi/"+o+"/SplitSplit/p_nWrongSplitIBLWrongSplitB_pT_"+o, "Frac Wrong Split IBL Wrong Split B") );
    CHECK ( bookHistogram(origin, "Hits/Combi/"+o+"/SplitSplitSplit/p_nSplitIBLSplitBSplitPix_pT_"+o, "Split IBL Split B #geq 1 Split Pix") );
    CHECK ( bookHistogram(origin, "Hits/Combi/"+o+"/SplitSplitSplit/p_nWrongSplitIBLWrongSplitBSplitPix_pT_"+o, "Wrong Split IBL Wrong Split B #geq 1 Split Pix") );
    CHECK ( bookHistogram(origin, "Hits/Combi/"+o+"/AllSplit/p_nAllSplitPix_pT_"+o, "Rate of tracks with all pixel hits split") );
    CHECK ( bookHistogram(origin, "Hits/Combi/"+o+"/AllSplit/p_fracWrongAllSplitPix_pT_"+o, "Frac tracks with all split pix with wrong IBL and B") );
    CHECK ( bookHistogram(origin, "Hits/Combi/"+o+"/AllSplit/p_fracWrongIBLAllSplitPix_pT_"+o, "Frac tracks with all split pix with wrong IBL") );
    CHECK ( bookHistogram(origin, "Hits/Combi/"+o+"/SplitSplitShared/p_nSplitIBLSplitBSharedPix_pT_"+o, "Split IBL Split B #geq 1 Shared Pix") );
    CHECK ( bookHistogram(origin, "Hits/Combi/"+o+"/SplitSplitShared/p_nWrongSplitIBLWrongSplitBSharedPix_pT_"+o, "Wrong Split IBL Wrong Split B #geq 1 Shared Pix") );
    CHECK ( bookHistogram(origin, "Hits/Combi/"+o+"/SharedSplit/p_nSharedIBLSplitB_pT_"+o, "Shared IBL Split B") );
    CHECK ( bookHistogram(origin, "Hits/Combi/"+o+"/SharedSplit/p_nWrongSharedIBLWrongSplitB_pT_"+o, "Frac Wrong Shared IBL Wrong Split B") );
    CHECK ( bookHistogram(origin, "Hits/Combi/"+o+"/SplitShared/p_nSplitIBLSharedB_pT_"+o, "Split IBL Shared B") );
    CHECK ( bookHistogram(origin, "Hits/Combi/"+o+"/SplitShared/p_nWrongSplitIBLWrongSharedB_pT_"+o, "Frac Wrong Split IBL Wrong Shared B") );
    CHECK ( bookHistogram(origin, "Hits/Combi/"+o+"/Wrong/p_nWrongIBLWrongB_pT_"+o, "Wrong IBL Wrong B") );
    CHECK ( bookHistogram(origin, "Hits/Res/GoodWrong/"+o+"/p_distToCorrectIBLCluster_pT_"+o, "Distance to Correct IBL Hit") );
    CHECK ( bookHistogram(origin, "Hits/Res/GoodWrong/"+o+"/p_distToCorrectBCluster_pT_"+o,   "Distance to Correct B Hit") );
    CHECK ( bookHistogram(origin, "Hits/Res/GoodWrong/"+o+"/p_distToCorrectPixCluster_pT_"+o, "Distance to Correct Pix Hit") );
    CHECK ( bookHistogram(origin, "Hits/Res/TrackWrong/"+o+"/p_distToTrackIBLCluster_pT_"+o, "Distance From Track to Wrong IBL Hit") );
    CHECK ( bookHistogram(origin, "Hits/Res/TrackWrong/"+o+"/p_distToTrackBCluster_pT_"+o,   "Distance From Track to Wrong B Hit") );
    CHECK ( bookHistogram(origin, "Hits/Res/TrackWrong/"+o+"/p_distToTrackPixCluster_pT_"+o, "Distance From Track to Wrong Pix Hit") );
    CHECK ( bookHistogram(origin, "Hits/Res/TruthMissing/"+o+"/p_distToMissingIBLCluster_pT_"+o, "Distance From Truth to Missing IBL Hit") );
    CHECK ( bookHistogram(origin, "Hits/Res/TruthMissing/"+o+"/p_distToMissingBCluster_pT_"+o,   "Distance From Truth to Missing B Hit") );
    CHECK ( bookHistogram(origin, "Hits/Res/TruthMissing/"+o+"/p_distToMissingPixCluster_pT_"+o, "Distance From Truth to Missing Pix Hit") );
    CHECK ( bookHistogram(origin, "Hits/Used/"+o+"/HitOnTrack/p_IBLHitUsedOnTrack_pT_"+o, "Fraction of All IBL Hits Used on Reco Tracks") );
    CHECK ( bookHistogram(origin, "Hits/Used/"+o+"/HitOnTrack/p_BHitUsedOnTrack_pT_"+o, "Fraction of All B Hits Used on Reco Tracks") );
    CHECK ( bookHistogram(origin, "Hits/Used/"+o+"/HitOnTrack/p_PixHitUsedOnTrack_pT_"+o, "Fraction of All Pix Hits Used on Reco Tracks") );
    CHECK ( bookHistogram(origin, "Hits/Used/"+o+"/HitOnTrack/p_Pix34HitUsedOnTrack_pT_"+o, "Fraction of All Pix34 Hits Used on Reco Tracks") );
    CHECK ( bookHistogram(origin, "Hits/Used/"+o+"/HitOnTrack/p_SCTHitUsedOnTrack_pT_"+o, "Fraction of All SCT Hits Used on Reco Tracks") );
    CHECK ( bookHistogram(origin, "Hits/Used/"+o+"/fracUnused/p_fracPixUnusedPerTrack_pT_"+o, "Fraction Unused Pix Per Track with Unused Hit") );
    CHECK ( bookHistogram(origin, "Hits/Used/"+o+"/fracUnused/p_fracPix34UnusedPerTrack_pT_"+o, "Fraction Unused Pix34 Per Track with Unused Hit") );
    CHECK ( bookHistogram(origin, "Hits/Used/"+o+"/fracUnused/p_fracSCTUnusedPerTrack_pT_"+o, "Fraction Unused SCT Per Track with Unused Hit") );
    CHECK ( bookHistogram(origin, "Hits/Used/"+o+"/fracTrack/p_fracTracksWithUnusedSi_pT_"+o, "Frac Tracks > 0 u. pix and > 0 u. SCT") );
    CHECK ( bookHistogram(origin, "Hits/Used/"+o+"/fracTrack/p_fracTracksWith1UnusedPix3UnusedSCT_pT_"+o, "Fraction Tracks with >0 u. pix and >3 u. SCT") );
    CHECK ( bookHistogram(origin, "Hits/Used/"+o+"/fracTrack/p_fracTracksWith2UnusedPix6UnusedSCT_pT_"+o, "Fraction Tracks with >1 u. pix and >5 u. SCT") );
    CHECK ( bookHistogram(origin, "Hits/Used/"+o+"/nUnusedPerUnreo/p_nPixUnusedPerUnreco_pT_"+o, "n unused pix hits per unreco track") );
    CHECK ( bookHistogram(origin, "Hits/Used/"+o+"/nUnusedPerUnreo/p_nSCTUnusedPerUnreco_pT_"+o, "n unused SCT hits per unreco track") );
  }
  // histograms for tracks from B or D Hadrons, split according to track origin
  // additional histograms for filling information using B or D hadron pT
  // (pT track plots are already covered above)
  for (const auto& hadron : m_hadrons) {
    auto o = "From_" + hadron;

    // fill per parent hadron
    id = "Kinematics/HadronDecays/"+o+"/h_nTruthDecays_"+o;
    CHECK( book( TH1D(id.c_str(), ("Number of Decay Particles Per "+hadron+" Hadron").c_str(), 20, 0.0, 20.0 ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(("Number of Decay Particles Per "+hadron+" Hadron").c_str()); hist(id.c_str())->GetYaxis()->SetTitle("Arbitrary Units");

    id = "Kinematics/HadronDecays/"+o+"/p_nTruthDecays_pT_"+o;
    CHECK( book( TProfile(id.c_str(), ("Number of Decay Particles Per "+hadron+" Hadron").c_str(), 20, 0.0, hpT_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(("p_{T}^{"+hadron+"} [GeV]").c_str()); hist(id.c_str())->GetYaxis()->SetTitle("Number of decay particles");

    id = "Kinematics/HadronDecays/"+o+"/p_nTruthDecays_DL_"+o;
    CHECK( book( TProfile(id.c_str(), ("Number of Decay Particles Per "+hadron+" Hadron").c_str(), 20, 0.0, PR_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle((hadron+" Hadron Decay Radius [mm]").c_str()); hist(id.c_str())->GetYaxis()->SetTitle("Number of decay particles");

    id = "Kinematics/HadronDecays/"+o+"/p_nRecoTracks_pT_"+o;
    CHECK( book( TProfile(id.c_str(), ("Number of Reconstructed Tracks Per "+hadron+" Hadron").c_str(), 20, 0.0, hpT_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(("p_{T}^{"+hadron+"} [GeV]").c_str()); hist(id.c_str())->GetYaxis()->SetTitle("Number of reconstructed decay particles");

    id = "Kinematics/HadronDecays/"+o+"/p_nRecoTracks_DL_"+o;
    CHECK( book( TProfile(id.c_str(), ("Number of Reconstructed Tracks Per "+hadron+" Hadron").c_str(), PR_nbins, 0.0, PR_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle((hadron+" Hadron Decay Radius [mm]").c_str()); hist(id.c_str())->GetYaxis()->SetTitle("Number of reconstructed decay particles");

    id = "Kinematics/HadronDecays/"+o+"/p_pTvsDL_"+o;
    CHECK( book( TProfile(id.c_str(), (hadron+" hadron pT vs decay length").c_str(), 20, 0.0, hpT_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(("p_{T}^{"+hadron+"} [GeV]").c_str()); hist(id.c_str())->GetYaxis()->SetTitle((hadron+" Hadron Decay Radius [mm]").c_str());

    id = "Kinematics/HadronDecays/"+o+"/p_averageDecayParticle_pT_"+o;
    CHECK( book( TProfile(id.c_str(), ("Average Decay Particle pT Per "+hadron+" Hadron").c_str(), 10, 0.0, hpT_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(("p_{T}^{"+hadron+"} [GeV]").c_str()); hist(id.c_str())->GetYaxis()->SetTitle("Decay Particle p_{T} [GeV]");

    id = "Kinematics/HadronDecays/"+o+"/p_averageDecayParticle_PR_"+o;
    CHECK( book( TProfile(id.c_str(), ("Average Decay Particle pT Per "+hadron+" Hadron").c_str(), 10, 0.0, hpT_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(("p_{T}^{"+hadron+"} [GeV]").c_str()); hist(id.c_str())->GetYaxis()->SetTitle("Stable Particle Production Radius [mm]");

    id = "Efficiency/"+o+"/p_recoEfficiency_pT"+hadron+"_"+o;
    CHECK( book( TProfile(id.c_str(), ("Fraction of "+hadron+" decays that are reconstructed").c_str(), 20, 0.0, hpT_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(("p_{T}^{"+hadron+"} [GeV]").c_str()); hist(id.c_str())->GetYaxis()->SetTitle("Reconstruction Efficiency");

    id = "Efficiency/"+o+"/p_recoEfficiency_dR_"+o;
    CHECK( book( TProfile(id.c_str(), ("Fraction of "+hadron+" decays that are reconstructed").c_str(), dR_nbins, 0.0, dR_max ) ) );
    hist(id.c_str())->GetXaxis()->SetTitle(("Stable Particle dR to "+hadron+" Hadron").c_str()); hist(id.c_str())->GetYaxis()->SetTitle("Reconstruction Efficiency");

    id = "TrackFit/"+o+"/p_ChiSquared_pT"+hadron+"_"+o;
    CHECK( book( TProfile(id.c_str(), ("#chi^{2} of track "+o).c_str(), hpT_nbins, 0.0, hpT_max ) ) );
    hist(id)->GetXaxis()->SetTitle(("p_{T}^{"+hadron+"} [GeV]").c_str()); hist(id)->GetYaxis()->SetTitle("#chi^{2}");

    id = "TrackFit/"+o+"/p_ChiSquared_dR_"+o;
    CHECK( book( TProfile(id.c_str(), ("#chi^{2} of track "+o).c_str(), dR_nbins, 0.0, dR_max ) ) );
    hist(id)->GetXaxis()->SetTitle(("Stable Particle dR to "+hadron+" Hadron").c_str()); hist(id)->GetYaxis()->SetTitle("#chi^{2}");

    id = "TrackFit/"+o+"/p_nDoF_pT"+hadron+"_"+o;
    CHECK( book( TProfile(id.c_str(), ("NDoF of track "+o).c_str(), hpT_nbins, 0.0, hpT_max ) ) );
    hist(id)->GetXaxis()->SetTitle(("p_{T}^{"+hadron+"} [GeV]").c_str()); hist(id)->GetYaxis()->SetTitle("Degrees of Freedom on Track");

    id = "TrackFit/"+o+"/p_nDoF_dR_"+o;
    CHECK( book( TProfile(id.c_str(), ("NDoF of track "+o).c_str(), dR_nbins, 0.0, dR_max ) ) );
    hist(id)->GetXaxis()->SetTitle(("Stable Particle dR to "+hadron+" Hadron").c_str()); hist(id)->GetYaxis()->SetTitle("Degrees of Freedom on Track");

    id = "TrackFit/"+o+"/p_ChiSquaredpernDoF_pT"+hadron+"_"+o;
    CHECK( book( TProfile(id.c_str(), ("#chi^2 per NDoF "+o).c_str(), hpT_nbins, 0.0, hpT_max ) ) );
    hist(id)->GetXaxis()->SetTitle(("p_{T}^{"+hadron+"} [GeV]").c_str()); hist(id)->GetYaxis()->SetTitle("#chi^{2}/n");

    id = "TrackFit/"+o+"/p_ChiSquaredpernDoF_dR_"+o;
    CHECK( book( TProfile(id.c_str(), ("#chi^2 per NDoF "+o).c_str(), dR_nbins, 0.0, dR_max ) ) );
    hist(id)->GetXaxis()->SetTitle(("Stable Particle dR to "+hadron+" Hadron").c_str()); hist(id)->GetYaxis()->SetTitle("#chi^{2}/n");

    id = "Kinematics/d0/"+o+"/p_d0_sig_size_pT"+hadron+"_"+o;
    CHECK( book( TProfile(id.c_str(), ("Track d0 sig size"+o).c_str(), hpT_nbins, 0.0, hpT_max ) ) );
    hist(id)->GetXaxis()->SetTitle(("p_{T}^{"+hadron+"} [GeV]").c_str()); hist(id)->GetYaxis()->SetTitle("|d_{0}|/#sigma(d_{0})");

    id = "Kinematics/d0/"+o+"/p_d0_pull_size_pT"+hadron+"_"+o;
    CHECK( book( TProfile(id.c_str(), ("Track d0 pull size"+o).c_str(), hpT_nbins, 0.0, hpT_max ) ) );
    hist(id)->GetXaxis()->SetTitle(("p_{T}^{"+hadron+"} [GeV]").c_str()); hist(id)->GetYaxis()->SetTitle("d_{0} pull size");
  }

  return StatusCode::SUCCESS;
}
