
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../trackAnalysisAlg.h"

DECLARE_ALGORITHM_FACTORY( trackAnalysisAlg )

DECLARE_FACTORY_ENTRIES( trackAnalysis ) 
{
  DECLARE_ALGORITHM( trackAnalysisAlg );
}
