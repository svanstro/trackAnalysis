import os

# ---- CONFIG -----
VERSION = 'get_eff_plots'
DATASET = 'Baseline'
OUTNAME = 'Nominal mu=40'
theApp.EvtMax = int(1e6)
#dataset_base_path = '/unix/atlas1/svanstroud/qt/aod/'
dataset_base_path = '/unix/atlasvhbb2/svanstroud/qt/aod/'
#dataset_base_path = '/unix/atlasvhbb2/srettie/tracking/'
# -----------------

# ---- INPUT DATA -----
datasets = {
    'Baseline'   : 'user.svanstro.qt.flatZprime.s3431.fullPUtruth.withpseudotracks.full_EXT0/',
    'newZ'       : 'group.perf-idtracking.427080.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime.e5362_e5984_s3126_r11212_IdealNomV5_EXT0/',
    'Candidates' : 'user.svanstro.qt.flatZprime.s3431.fullPUtruth.withcandidatesAndPseudotracks.full_EXT0/',
    'GX2F-q-1.5' : 'user.svanstro.qt.flatZprime.s3431.fullPUtruth.pseudo.IBLcutat1.5.IBLcut2at0.quad.chi2cutat4.v3.RealQuadrature_EXT0',
    'GX2F-q-1.0' : 'user.svanstro.qt.flatZprime.s3431.fullPUtruth.pseudo.IBLcutat1.25.IBLcut1at1.75cut2at0.chi2cutat4.RealQ_EXT0/',
}

# check we know the DID for this variant
assert DATASET in datasets, 'cannot find DSID for DATASET: "'+DATASET+'".'

# get the path to the local DID download location
input_dir = os.path.join(dataset_base_path, datasets[DATASET])

# get the .root files that are stored locally under this DID
input_files = []
for file in os.listdir(input_dir):
    if file.endswith('.root'): # ignore .part files and other
        input_files.append(os.path.join(input_dir, file))

jps.AthenaCommonFlags.FilesInput = input_files
#jps.AthenaCommonFlags.FilesInput = ['/unix/atlas1/svanstroud/qt/reconstruction/compile-TrackStateOnSurfaceDecorator/run/HitInfo.root'] # manual override with test file
# -----------------


# ---- OUTPUT DATA -----
# make the output directories so the hist service can register files
if not os.path.isdir(os.path.join('output', VERSION)):
    os.mkdir(os.path.join('output', VERSION))
# -----------------



#---- Minimal job options -----
jps.AthenaCommonFlags.AccessMode = "ClassAccess"              #Choose from TreeAccess,BranchAccess,ClassAccess,AthenaAccess,POOLAccess
svcMgr.THistSvc.MaxFileSize = -1 # potentially speeds up 

hist_outFileName = os.path.join('output', VERSION,OUTNAME+'.root')
tree_outFileName = os.path.join('dump', 'trackdump.root')

#svcMgr += CfgMgr.THistSvc()
jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:"+hist_outFileName]   # register output files like this. MYSTREAM is used in the code
jps.AthenaCommonFlags.HistOutputs = ["TREESTREAM:"+tree_outFileName] # no functionality yet, but could set a flag to dump track info into a ntuple for 3rd party analysis
#svcMgr.THistSvc.Output += ["HISTSTREAM='"+hist_outFileName+"' OPT='RECREATE'"]
#svcMgr.THistSvc.Output += ["TREESTREAM='"+tree_outFileName+"' OPT='RECREATE'"]
#svcMgr.THistSvc.Output += ["TREESTREAM='anotherfile.root'"]


#athAlgSeq += CfgMgr.trackAnalysisAlg()                               #adds an instance of your alg to the main alg sequence
athAlgSeq += CfgMgr.trackAnalysisAlg(OutputLevel=ALL)      # for logging
#athAlgSeq += CfgMgr.trackAnalysisAlg(OutputLevel=DEBUG)      # for logging



#---- Options you could specify on command line -----
#jps.AthenaCommonFlags.EvtMax=-1                          #set on command-line with: --evtMax=-1
#jps.AthenaCommonFlags.SkipEvents=0                       #set on command-line with: --skipEvents=0
#jps.AthenaCommonFlags.FilesInput = ["/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/CommonInputs/DAOD_PHYSVAL/mc16_13TeV.410501.PowhegPythia8EvtGen_A14_ttbar_hdamp258p75_nonallhad.DAOD_PHYSVAL.e5458_s3126_r9364_r9315_AthDerivation-21.2.1.0.root"]        #set on command-line with: --filesInput=...

# ---- LOGGING -----
include("AthAnalysisBaseComps/SuppressLogging.py")              #Optional include to suppress as much athena output as possible. Keep at bottom of joboptions so that it doesn't suppress the logging of the things you have configured above
# -----------------

