#!/usr/bin/env python
# -*- coding: utf-8 -*-

# imports
from __future__ import print_function

# stdlib imports
from argparse import ArgumentParser
import os
import datetime
import csv 
import re
import math

from distutils.dir_util import copy_tree, remove_tree

# root
print('importing ROOT......')
import ROOT # need full import for error level setting

ROOT.gROOT.LoadMacro("AtlasUtils.C")

# suppress info messages
ROOT.gErrorIgnoreLevel = 3000

# don't display plots
ROOT.gROOT.SetBatch(True)

# don't show hist title
ROOT.gStyle.SetOptTitle(0)

c_dict = {'Baseline': 1,
          'Pseudo-tracks': 861,
          'Reco-Pseudo': 861,
          'RecoPseudo-tracks': 46,
          'Mod GX2F':46,
          'Candidates': 801}

colours = [861, 801, 8,  46, 617 , 857, 28, 53, 87, 91] 

def process_plot(plot_objects, path_to_subdir, names, all_file_types=False):

    canvas = ROOT.TCanvas( 'c1', 'this canvas', 100, 100, int(550), int(500))
    canvas.SetRightMargin(0.055);
    canvas.SetLeftMargin(0.12)
    canvas.SetTopMargin(0.05)

    pad = canvas.cd()

    pad.SetTickx()
    pad.SetTicky()
    
    plot_titles = plot_objects[0].GetTitle() 
    plot_titles += (';' + plot_objects[0].GetXaxis().GetTitle()) 
    plot_titles += (';' + plot_objects[0].GetYaxis().GetTitle())

    [plot_object.SetMarkerColor(colours[c]) for c, plot_object in enumerate(plot_objects)]
    [plot_object.SetLineColor(colours[c]) for c, plot_object in enumerate(plot_objects)]

    stack = ROOT.THStack(plot_objects[0].GetName(), plot_titles)

    ROOT.gStyle.SetTitleY(0.955)
    ROOT.gStyle.SetTitleX(0.53)

    [plot_object.SetTitle(names[n]) for n, plot_object in enumerate(plot_objects)]
    [plot_object.SetMarkerSize(1.0) for plot_object in plot_objects]

    #[p.SetMarkerColor(c_dict[names[i]])
    #[p.SetLineColor(c_dict[names[i]])

    for i, plot_object in enumerate(plot_objects):
        if names[i] in c_dict:
            plot_object.SetMarkerColor(c_dict[names[i]])
            plot_object.SetLineColor(c_dict[names[i]])



    density = True  
    if density and plot_objects[0].ClassName() == "TH1D":
        if not '__' in plot_objects[0].GetName():
            #[plot_object.Scale(1/plot_object.GetEntries()) if plot_object.GetEntries() != 0 else 0 for plot_object in plot_objects]
            [plot_object.Scale(1/plot_object.Integral(0,plot_objects[0].GetNbinsX()+1)) if plot_object.Integral(0,plot_objects[0].GetNbinsX()+1) != 0 else 0 for plot_object in plot_objects]


    [stack.Add(plot_object) for plot_object in plot_objects]

    if plot_objects[0].ClassName() == "TH1D":
        stack.Draw('nostack HIST')
    else:
        stack.Draw('nostack')

    #stack.GetXaxis().SetTitleOffset(1.2)
    #stack.GetYaxis().SetTitleOffset(1.4)

    stack.GetXaxis().SetTitleOffset(1.05)
    stack.GetYaxis().SetTitleOffset(1.1)

    # can improve by reducing the granularity at small ylim and increasing at large ylim
    stack.SetMaximum(get_quantized_ylim(plot_objects))

    stack.GetXaxis().SetTitleSize(0.045)
    stack.GetYaxis().SetTitleSize(0.045)

    # ------------------------------------------------------------
    # legend styling
    # slides
    legend = canvas.BuildLegend(0.6,0.75,0.90,0.90) # for 2 legend entries
    
    # for report
    #legend = canvas.BuildLegend(0.5,0.7,0.905,0.9) # three entries
    #legend = canvas.BuildLegend(0.5,0.75,0.9,0.9)
    #legend = canvas.BuildLegend(0.65,0.8,0.905,0.9) # two entries

    legend.SetBorderSize(0)
    #[legend.AddEntry(plot_object, names[n], 'lp') for n, plot_object in enumerate(plot_objects)]
    #legend.AddEntry(plot_object_1, A_NAME, 'lp')
    #legend.AddEntry(plot_object_2, B_NAME, 'lp')
    #legend.SetTextSize(0.03); # report
    #legend.SetTextSize(0.05); # slides for 2
    legend.SetTextSize(0.045); # slides for 3
    legend.Draw('SAME')
    #canvas.BuildLegend(600/800,450/600,700/800,550/600);
    # ------------------------------------------------------------

    draw_label(plot_objects[0])

    canvas.Print(os.path.join(path_to_subdir, plot_objects[0].GetName() + '.pdf'))
    canvas.Close()


def draw_label(plot_obj):
    for_presentation = True

    if 'From_' in plot_obj.GetName():
        origin = plot_obj.GetName().split('From_')[1].split('/')[0]
    else:
        return

    if for_presentation:

        if origin == 'Other':
            origin = 'Primary Tracks'
        else:
            origin = origin + ' Tracks'

        l = ROOT.TLatex()
        l.SetNDC()
        l.SetTextFont(72)
        l.SetTextSize(0.05)
        l.SetTextColor(ROOT.kBlack)
        l.DrawLatex(0.175,0.85, origin)


    else:
        l = ROOT.TLatex()
        l.SetNDC()
        l.SetTextFont(72)
        l.SetTextSize(0.04)
        l.SetTextColor(ROOT.kBlack)
        l.DrawLatex(0.15,0.875, "ATLAS")

        l = ROOT.TLatex()
        l.SetNDC()
        l.SetTextFont(42)
        l.SetTextSize(0.04)
        l.SetTextColor(ROOT.kBlack)
        l.DrawLatex(0.27,0.875, "Internal")

        if 'From_' in plot_obj.GetName():
            origin = plot_obj.GetName().split('From_')[1]
            if origin == "B" or origin == "D":
                origin = 'Tracks from ' + origin + ' Hadrons'
            elif origin == 'Other':
                origin = 'Primary Tracks'
            else:
                origin = origin + ' Tracks'

            l = ROOT.TLatex()
            l.SetNDC()
            l.SetTextFont(42)
            l.SetTextSize(0.03)
            l.SetTextColor(ROOT.kBlack)
            l.DrawLatex(0.15,0.84, origin)


def get_quantized_ylim(plot_objects):
    if plot_objects[0].ClassName() == 'TH1D':
        return max([p.GetMaximum() for p in plot_objects])*1.25

    # get plot object with highest average y val
    max_y = 0
    max_mean_y = 0
    for p in plot_objects:
        mean_y = 0
        nbins = p.GetNbinsX()

        for i in range(nbins+1):
            y_val = p.GetBinContent(i)
            mean_y += y_val / nbins

            if y_val > max_y:
                max_y = y_val

        if mean_y > max_mean_y:
            max_mean_y = mean_y 

    if max_y == 0:
        digits = 0
    else:
        power = math.log(max_y,2)
        if ( power % 1 < 0.1):
            digits = round(power)
        else:
            digits = math.ceil(power)

    #print('max y = ', max_y)
    #print('digits = ', digits)


    #ylim = math.ceil(max_y*area)/area
    ylim=1
    if max_y != 0 and digits!=0:
        #ylim = round(max_y, int(math.ceil(math.log(max_y,10))) )
        #ylim = round(max_y, 1/digits)
        ylim = math.ceil(max_y/(2**digits))*(2**digits)
        #print('ylim = ', ylim)
    return ylim*1.35




    # get the range that we are in and set quantization

    # set y lim



#def process_subdir(subdir_object_1, subdir_object_2, path_to_subdir):
def process_subdir(subdirs, path_to_subdir, names):


    list_for_each = [subdir.GetListOfKeys() for subdir in subdirs]

    read_list = [list(list_) for list_ in list_for_each]


    zipped = list(zip(*read_list))

    #print(zipped[0])

    #for these_items in zip([subdir.GetListOfKeys() for subdir in subdirs]):
    for these_items in zipped:

        these_items = [this_item.ReadObj() for this_item in these_items]

        # if we are currently looking at plots
        if these_items[0].ClassName() == 'TH1D' or these_items[0].ClassName() == 'TProfile':

            if not all([i.ClassName() == these_items[0].ClassName() for i in these_items]):
                [print(names[n] +' - - ' +  i.GetName()) for n,i in enumerate(these_items)]
            
            assert all([i.ClassName() == these_items[0].ClassName() for i in these_items]), "ROOT files don't match at " + these_items[0].GetName() + ' in ' + names[0]

            process_plot(these_items, path_to_subdir, names)

        elif these_items[0].ClassName() == 'TDirectoryFile':

            these_dirs = these_items

            print('Looking in', these_dirs[0].ClassName(), 'called', path_to_subdir+'/'+these_dirs[0].GetName())

            this_subdir = os.path.join(path_to_subdir, these_dirs[0].GetName())

            os.mkdir(this_subdir)

            # special cases (should be a flag in filename we can detect)
            collapse_dirs = ['collapse']
            overlay_dirs = ['Overlay']

            #if these_dirs[0].GetName() in collapse_dirs:
                # plot all the histograms in this dir on top of each other 
                # separately for each input file
                #pass # fix later
                #collapse_plot(these_dirs, this_subdir, names)
            #elif these_dirs[0].GetName() in overlay_dirs:
                #continue
                #overlay_plot(these_dirs, this_subdir, names, relative=True)
                #overlay_plot(these_dirs, this_subdir, names, relative=False)
            #else:
            process_subdir(these_dirs, this_subdir, names)



def process_root_files(filenames, outpath):

    # open the file
    tfiles = [ROOT.TFile.Open(filename) for filename in filenames]
    
    # not sure why
    [tfile.cd() for tfile in tfiles]

    # get the names of each
    names = [filename.split('/')[-1][:-5] for filename in filenames]

    # recursively traverse dirs in the root files
    process_subdir(tfiles, outpath, names)
    




def copy_dir(from_dir, to_dir):

     # check availbility
    if os.path.isdir(to_dir):
        print('Copying ' + from_dir + ' to ' + to_dir)
        copy_tree(from_dir, to_dir)
    else:
        print('Failed to find available base directory at '+ to_dir)


def make_all_plots():
    # write to this location
    outpath = './plots'

    # check availbility
    if os.path.isdir(outpath):
        print('Using base directory at ' + outpath)
    else:
        print('Failed to find available base directory at ' + outpath + '. Exiting...')
        return

    # handle argument
    # todo, make this a dir and then plot all the files in the dir
    parser = ArgumentParser()
    parser.add_argument("directory", help="Directory containing several ROOT files to be overlaid")
    args = parser.parse_args()

    dir_category = args.directory.split('/')[-2]



    # list contents of dir
    filenames = sorted(os.listdir(args.directory), reverse=True)

    # select ROOT files
    filenames = [os.path.join(args.directory, filename) for filename in filenames if filename.endswith('.root')]

    # check we have something to plot
    #if len(top_dir) == 0:
    assert len(filenames) != 0, 'No files found! Exiting...'

    # create top level dir if necessary
    if not os.path.isdir(os.path.join(outpath, dir_category)):
        print('Creating base directory at ' + os.path.join(outpath, dir_category))
        os.mkdir(os.path.join(outpath, dir_category))


    # get a timestamp for the dir name
    st = str(datetime.datetime.now()).split('.')[0]
    print(st)
    st = st.replace(':', '.')
    st = st.replace(' ', '_')
    print(st)
    save_path = os.path.join(outpath, dir_category, st)

    # create the save path
    os.mkdir(save_path)
    print('Created new directory at ', save_path)

    # process file
    process_root_files(filenames, save_path)



if __name__ == '__main__':
    make_all_plots()







