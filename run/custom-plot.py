#!/usr/bin/env python
# -*- coding: utf-8 -*-

# imports
from __future__ import print_function

# stdlib imports
from argparse import ArgumentParser
import os
import datetime
import csv 
import re
import math

from distutils.dir_util import copy_tree, remove_tree

# root
print('importing ROOT......')
import ROOT # need full import for error level setting

ROOT.gROOT.LoadMacro("AtlasUtils.C")

# suppress info messages
ROOT.gErrorIgnoreLevel = 3000

# don't display plots
ROOT.gROOT.SetBatch(True)

# don't show hist title
ROOT.gStyle.SetOptTitle(0)


c_dict = {'Baseline': 1,
          'Pseudo-tracks': 861,
          'Reco-Pseudo': 46,
          'RecoPseudo-tracks': 46,
          'Mod GX2F':46,
          'Candidates': 801}


# --- for reco efficiency
# version name
key = 'final'

# file names
baseline = ROOT.TFile.Open('output/'+key+'/Baseline.root')
pseudo   = ROOT.TFile.Open('output/'+key+'/Pseudo-tracks.root')
cand     = ROOT.TFile.Open('output/'+key+'/Candidates.root')

# hist names
hists = []
hists += [baseline.Get('Efficiency/From_B/p_recoEfficiency_pT_From_B')]
hists += [baseline.Get('Efficiency/From_Other/p_recoEfficiency_pT_From_Other')]
hists += [pseudo.Get('Efficiency/From_B/p_recoEfficiency_pT_From_B')]
hists += [cand.Get('Efficiency/From_B/p_recoEfficiency_pT_From_B')]

print(hists)

canvas = ROOT.TCanvas( 'c1', 'this canvas', 100, 100, int(550), int(500))
canvas.SetRightMargin(0.055);
canvas.SetLeftMargin(0.12)
canvas.SetTopMargin(0.05)
pad = canvas.cd()
pad.SetTickx()
pad.SetTicky()


hists[0].SetTitle('Tracks From B')
hists[1].SetTitle('Primary Tracks')
hists[2].SetTitle('Pseudo-tracks')
hists[3].SetTitle('Candidates From B')
#hists[4].SetTitle('Primary Candidates')


plot_titles = hists[0].GetTitle() 
plot_titles += (';' + hists[0].GetXaxis().GetTitle()) 
plot_titles += (';' + hists[0].GetYaxis().GetTitle())

stack = ROOT.THStack(hists[0].GetName(), plot_titles)

ROOT.gStyle.SetTitleY(0.955)
ROOT.gStyle.SetTitleX(0.53)

[plot_object.SetMarkerSize(1.0) for plot_object in hists]


hists[0].SetMarkerColor(1)
hists[0].SetLineColor(1)

hists[1].SetMarkerColor(46)
hists[1].SetLineColor(46)
hists[1].SetMarkerStyle(22)
#hists[1].SetMarkerSize(1.0)

hists[2].SetMarkerColor(861)
hists[2].SetLineColor(861)

hists[3].SetMarkerColor(801)
hists[3].SetLineColor(801)


#hists[4].SetMarkerColor(46)
#hists[4].SetLineColor(46)
#hists[4].SetMarkerStyle(26)
#hists[4].SetMarkerSize(1.0)

[stack.Add(plot_object) for plot_object in hists]

if hists[0].ClassName() == "TH1D":
    stack.Draw('nostack HIST')
else:
    stack.Draw('nostack')

stack.GetXaxis().SetTitleOffset(1.2)
stack.GetYaxis().SetTitleOffset(1.4)
stack.SetMaximum(1.35)

legend = canvas.BuildLegend(0.55,0.75,0.90,0.90) # for 2 legend entries
legend.SetBorderSize(0)
legend.SetTextSize(0.04); # slides for 3
legend.Draw('SAME')

if not os.path.isdir('plots/'+key+'/custom/'):
    os.mkdir('plots/'+key+'/custom/')
canvas.Print(os.path.join('plots/'+key+'/custom/', hists[0].GetName() + '.pdf'))
canvas.Close()



# --- for cluster counts
for det in ['IBL', 'Pix']:
    for v in ['pT', 'pTB']:
        hists = []
        hists += [baseline.Get('Hits/nOnCluster/From_B/Good/p_nTruthOnGood'+det+'Cluster_'+v+'_From_B')]
        hists += [baseline.Get('Hits/nOnCluster/From_B/Wrong/p_nTruthOnWrong'+det+'Cluster_'+v+'_From_B')]
        hists += [baseline.Get('Hits/nOnCluster/From_B/Missing/p_nTruthOnMissing'+det+'Cluster_'+v+'_From_B')]

        print(hists)

        canvas = ROOT.TCanvas( 'c1', 'this canvas', 100, 100, int(550), int(500))
        canvas.SetRightMargin(0.055);
        canvas.SetLeftMargin(0.12)
        canvas.SetTopMargin(0.05)
        pad = canvas.cd()
        pad.SetTickx()
        pad.SetTicky()

        hists[0].SetTitle('Good Cluster')
        hists[1].SetTitle('Wrong Cluster')
        hists[2].SetTitle('Missing Cluster')

        #[h.Rebin(2) for h in hists]

        plot_titles = hists[0].GetTitle() 
        plot_titles += (';' + hists[0].GetXaxis().GetTitle()) 
        plot_titles += ('; Truth Particles On '+det+' Cluster')

        stack = ROOT.THStack(hists[0].GetName(), plot_titles)

        ROOT.gStyle.SetTitleY(0.955)
        ROOT.gStyle.SetTitleX(0.53)



        [plot_object.SetMarkerSize(1.0) for plot_object in hists]

        
        hists[0].SetMarkerColor(1)
        hists[0].SetLineColor(1)

        hists[1].SetMarkerColor(46)
        hists[1].SetLineColor(46)

        hists[2].SetMarkerColor(861)
        hists[2].SetLineColor(861)

        [stack.Add(plot_object) for plot_object in hists]

        if hists[0].ClassName() == "TH1D":
            stack.Draw('nostack HIST')
        else:
            stack.Draw('nostack')

        stack.GetXaxis().SetTitleOffset(1.2)
        stack.GetYaxis().SetTitleOffset(1.4)
        stack.SetMaximum(10)

        legend = canvas.BuildLegend(0.55,0.75,0.90,0.90) # for 2 legend entries
        legend.SetBorderSize(0)
        legend.SetTextSize(0.04); # slides for 3
        legend.Draw('SAME')
        draw_label(hists[0])

        if not os.path.isdir('plots/'+key+'/custom/'):
            os.mkdir('plots/'+key+'/custom/')
        canvas.Print(os.path.join('plots/'+key+'/custom/', hists[0].GetName() + '.pdf'))
        canvas.Close()





