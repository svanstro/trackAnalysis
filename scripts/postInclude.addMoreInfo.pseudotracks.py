from AthenaCommon.GlobalFlags import globalflags
from InDetRecExample.InDetJobProperties import InDetFlags
InDetFlags.doTruth = (globalflags.DataSource == 'geant4' and globalflags.InputFormat() == 'pool')

# add pixel hit info
from InDetPrepRawDataToxAOD.InDetPrepRawDataToxAODConf import PixelPrepDataToxAOD
xAOD_PixelPrepDataToxAOD = PixelPrepDataToxAOD( name = "xAOD_PixelPrepDataToxAOD")
xAOD_PixelPrepDataToxAOD.OutputLevel=INFO
xAOD_PixelPrepDataToxAOD.UseTruthInfo = True
print "Add Pixel xAOD PrepRawData:"
print xAOD_PixelPrepDataToxAOD
topSequence += xAOD_PixelPrepDataToxAOD

# add SCT hit info
from InDetPrepRawDataToxAOD.InDetPrepRawDataToxAODConf import SCT_PrepDataToxAOD
xAOD_SCT_PrepDataToxAOD = SCT_PrepDataToxAOD( name = "xAOD_SCT_PrepDataToxAOD")
xAOD_SCT_PrepDataToxAOD.OutputLevel=INFO
xAOD_SCT_PrepDataToxAOD.UseTruthInfo=True
print "Add SCT xAOD TrackMeasurementValidation:"
print xAOD_SCT_PrepDataToxAOD
topSequence += xAOD_SCT_PrepDataToxAOD

augmentationTools = []

# add msos to reco tracks
from DerivationFrameworkInDet.DerivationFrameworkInDetConf import DerivationFramework__TrackStateOnSurfaceDecorator
DFTSOS = DerivationFramework__TrackStateOnSurfaceDecorator(name = "DFTrackStateOnSurfaceDecorator",
                                                           ContainerName = InDetKeys.xAODTrackParticleContainer(),
                                                           DecorationPrefix = "",
                                                           TRT_ToT_dEdx = "",
                                                           StoreTRT = False,
                                                           OutputLevel =INFO)
ToolSvc += DFTSOS
augmentationTools += [DFTSOS]

# add msos to pseudo tracks
DFTSOS_PT = DerivationFramework__TrackStateOnSurfaceDecorator(name = "DFPTTrackStateOnSurfaceDecorator",
                                                           ContainerName = InDetKeys.xAODPseudoTrackParticleContainer(),
                                                           DecorationPrefix = "Pseudo_",
                                                           PixelMsosName = "Pseudo_Pixel_MSOSs",
                                                           SctMsosName = "Pseudo_SCT_MSOSs",
                                                           TrtMsosName = "Pseudo_TRT_MSOSs",
                                                           TRT_ToT_dEdx = "",
                                                           StoreTRT = False,
                                                           OutputLevel =INFO)
ToolSvc += DFTSOS_PT
augmentationTools += [DFTSOS_PT]


# Add decoration with truth parameters if running on simulation
from DerivationFrameworkInDet.DerivationFrameworkInDetConf import DerivationFramework__TrackParametersForTruthParticles
TruthDecor = DerivationFramework__TrackParametersForTruthParticles(name = "TruthTPDecor",
                                                                   DecorationPrefix = "truth_")
ToolSvc += TruthDecor
augmentationTools += [TruthDecor]
print TruthDecor

                   
                   
DerivationFrameworkJob = CfgMgr.AthSequencer("MySeq2")
from DerivationFrameworkCore.DerivationFrameworkCoreConf import DerivationFramework__CommonAugmentation
DerivationFrameworkJob += CfgMgr.DerivationFramework__CommonAugmentation("DFTSOS_KERN",
                                                                         AugmentationTools = augmentationTools,
                                                                         OutputLevel =INFO)
                                                 
topSequence += DerivationFrameworkJob



# build a new output stream to put the info we need
from OutputStreamAthenaPool.MultipleStreamManager import MSMgr
from D2PDMaker.D2PDHelpers import buildFileName

fileName   = buildFileName( primDPD.WriteDAOD_IDTIDEStream )
HitInfoStream = MSMgr.NewPoolRootStream( "HitInfo", "HitInfo.root" )

HitInfoStream.AddItem("xAOD::EventInfo#*")
HitInfoStream.AddItem("xAOD::EventAuxInfo#*")

HitInfoStream.AddItem("xAOD::TruthEventContainer#*")
HitInfoStream.AddItem("xAOD::TruthEventAuxContainer#*")
HitInfoStream.AddItem("xAOD::TruthPileupEventContainer#*")
HitInfoStream.AddItem("xAOD::TruthPileupEventAuxContainer#*")
HitInfoStream.AddItem("xAOD::TruthParticleContainer#*")
HitInfoStream.AddItem("xAOD::TruthParticleAuxContainer#")
HitInfoStream.AddItem("xAOD::TruthVertexContainer#*")
HitInfoStream.AddItem("xAOD::TruthVertexAuxContainer#*")

HitInfoStream.AddItem("xAOD::TrackParticleContainer#InDetTrackParticles")
HitInfoStream.AddItem("xAOD::TrackParticleAuxContainer#InDetTrackParticlesAux.-caloExtension.-cellAssociation.-clusterAssociation.-trackParameterCovarianceMatrices.-parameterX.-parameterY.-parameterZ.-parameterPX.-parameterPY.-parameterPZ.-parameterPosition")
HitInfoStream.AddItem("xAOD::TrackMeasurementValidationContainer#*")
HitInfoStream.AddItem("xAOD::TrackMeasurementValidationAuxContainer#*")
HitInfoStream.AddItem("xAOD::TrackStateValidationContainer#*")
HitInfoStream.AddItem("xAOD::TrackStateValidationAuxContainer#*")
HitInfoStream.AddItem("xAOD::TrackParticleClusterAssociationContainer#*")
HitInfoStream.AddItem("xAOD::TrackParticleClusterAssociationAuxContainer#*")
HitInfoStream.AddItem("xAOD::VertexContainer#PrimaryVertices")
HitInfoStream.AddItem("xAOD::VertexAuxContainer#PrimaryVerticesAux.-vxTrackAtVertex")

# for pseudo-tracks
HitInfoStream.AddItem("xAOD::TrackParticleContainer#InDetPseudoTrackParticles")
HitInfoStream.AddItem("xAOD::TrackParticleAuxContainer#InDetPseudoTrackParticlesAux.-caloExtension.-cellAssociation.-clusterAssociation.-trackParameterCovarianceMatrices.-parameterX.-parameterY.-parameterZ.-parameterPX.-parameterPY.-parameterPZ.-parameterPosition")


""" 
# Other things:
HitInfoStream.AddItem("xAOD::BTaggingContainer#*-HLTBjetFex")
HitInfoStream.AddItem("xAOD::BTaggingAuxContainer#*-HLTBjetFexAux")
HitInfoStream.AddItem("xAOD::JetContainer#AntiKt4EMTopoJets")
HitInfoStream.AddItem("xAOD::JetContainer#AntiKt2PV0TrackJets")
HitInfoStream.AddItem("xAOD::JetContainer#AntiKt4TruthJets")
HitInfoStream.AddItem("xAOD::JetAuxContainer#AntiKt4EMTopoJetsAux.")
HitInfoStream.AddItem("xAOD::JetAuxContainer#AntiKt2PV0TrackJetsAux.")
HitInfoStream.AddItem("xAOD::JetAuxContainer#AntiKt4TruthJetsAux.")
HitInfoStream.AddItem("xAOD::EventShape#*-HLTKt4LCTopoEventShape")
HitInfoStream.AddItem("xAOD::EventShapeAuxInfo#*-HLTKt4LCTopoEventShapeAux")
HitInfoStream.AddItem("xAOD::TriggerMenuContainer#*")
HitInfoStream.AddItem("xAOD::TriggerMenuContainer#TriggerMenu")
HitInfoStream.AddItem("xAOD::TriggerMenuAuxContainer#*")
HitInfoStream.AddItem("xAOD::TriggerMenuAuxContainer#TriggerMenuAux.")
HitInfoStream.AddItem("xAOD::TrigConfKeys#*")
HitInfoStream.AddItem("xAOD::TrigDecision#*")
HitInfoStream.AddItem("xAOD::TrigDecisionAuxInfo#*")
HitInfoStream.AddItem("xAOD::BTagVertexContainer#*-BjetVertexFex")
HitInfoStream.AddItem("xAOD::BTagVertexAuxContainer#*-BjetVertexFexAux")
HitInfoStream.AddItem("xAOD::ElectronContainer#Electrons")
HitInfoStream.AddItem("xAOD::ElectronAuxContainer#ElectronsAux.")
HitInfoStream.AddItem("xAOD::PhotonContainer#Photons")
HitInfoStream.AddItem("xAOD::PhotonAuxContainer#PhotonsAux.")
HitInfoStream.AddItem("xAOD::MuonContainer#Muons")
HitInfoStream.AddItem("xAOD::MuonAuxContainer#MuonsAux.")
HitInfoStream.AddItem("xAOD::TauJetContainer#TauJets")
HitInfoStream.AddItem("xAOD::TauJetAuxContainer#TauJetsAux.")
"""

