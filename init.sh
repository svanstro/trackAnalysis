#!/bin/bash
setupATLAS

mkdir build
mkdir run/plots
mkdir run/output
mkdir run/logs
mkdir run/dump

cd build
acmSetup AthAnalysis,21.2.100
acm new_skeleton trackAnalysis
acm compile

cd ../run

